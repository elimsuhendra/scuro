@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#summary" data-toggle="tab" aria-expanded="true">Summary</a>
          </li>
          <li>
            <a href="#cs_group" data-toggle="tab" aria-expanded="false">Customer Group</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="summary">
            <div class="row">
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'voucher_code','label' => 'Voucher Code','value' => (old('voucher_code') ? old('voucher_code') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'voucher_name','label' => 'Voucher Name','value' => (old('voucher_name') ? old('voucher_name') : ''),'attribute' => 'required','form_class' => 'col-md-6'])!!}
            </div>
            {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : '')])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_time','label' => 'Valid Time','value' => (old('valid_time') ? old('valid_time') : ''),'attribute' => 'readonly required','class' => 'datetimerange'])!!}
            {!!view($view_path.'.builder.select',['type' => 'select','name' => 'discount_type', 'data' => ['v' => 'Value', 'p' => 'Percentage'],'value' => 'v','label' => 'Discount Type','class' => 'select2'])!!}
            {!!view($view_path.'.builder.text',['type' => 'number','name' => 'amount','label' => 'Amount','value' => (old('amount') ? old('amount') : '')])!!}
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','value' => (old('status') ? old('status') : '')])!!}
          </div>
          <div class="tab-pane" id="cs_group">
            <div class="row">
              {!!view($view_path.'.builder.text',['type' => 'number','name' => 'min_checkout','label' => 'Minimal Purchase To Use Voucher','value' => (old('min_checkout') ? old('min_checkout') : ''),'form_class' => 'col-md-4'])!!}
              {!!view($view_path.'.builder.text',['type' => 'number','name' => 'total','label' => 'Total Voucher','value' => (old('total') ? old('total') : ''),'form_class' => 'col-md-4'])!!}
              {!!view($view_path.'.builder.text',['type' => 'number','name' => 'total_use_per_user','label' => 'Total Use Voucher Per User','value' => (old('total_use_per_user') ? old('total_use_per_user') : ''),'form_class' => 'col-md-4'])!!}
            </div>
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Yes','n' => 'No'],'name' => 'free_shipping','label' => 'Free Shipping','value' => (old('free_shipping') ? old('free_shipping') : 'n')])!!}
           <!--  {!!view($view_path.'.builder.checkbox',['type' => 'checkbox','name' => 'voucher_customer_group_data','label' => 'Apply To Customer Group','data' => $customer_group,'note' => 'If not checked, this will apply to all user'])!!} -->
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
    <script>

    </script>
@endpush
@endsection
