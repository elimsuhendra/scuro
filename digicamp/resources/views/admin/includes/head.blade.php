<meta charset="utf-8" />
<title>{{$title}} - AdminCorner</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="id">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta name="robots" content="noindex">

<meta content="{{$web_keywords}}" name="keywords" />
<meta content="{{$web_description}}" name="description" />
<meta content="Vincent" name="author" />
<meta name="geo.placename" content="Indonesia">
<meta name="geo.country" content="ID">
<meta name="language" content="Indonesian">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="root_url" content="{{url($root_path)}}/" />

<!-- Core CSS -->
<link rel="icon" href="{{asset('components/both/images/web')}}/{{$favicon}}" type="image/x-icon">
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->

<link rel = "stylesheet" href="{{asset('components/plugins/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel = "stylesheet" href="{{asset('components/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css">
<link rel = "stylesheet" href="{{asset('components/back/css/components-md.min.css')}}" type="text/css">
<link rel = "stylesheet" href="{{asset('components/back/css/plugins-md.min.css')}}" type="text/css">
<link rel = "stylesheet" href="{{asset('components/back/css/layout.min.css')}}" type="text/css">
<link rel = "stylesheet" href="{{asset('components/back/css/light.min.css')}}" type="text/css">
@stack('css')
<link rel = "stylesheet" href="{{asset('components/plugins/jquery-ui/jquery-ui.min.css')}}" type="text/css">
<link href="{{asset('components/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/sweetalert2/sweet-alert.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/bootstrap-colorpicker/css/colorpicker.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('components/plugins/cropper/cropper.min.css')}}">
<link rel = "stylesheet" href="{{asset('components/plugins/magnific/magnific-popup.css')}}">
<link rel = "stylesheet" href="{{asset('components/back/css/style.css')}}" type="text/css">