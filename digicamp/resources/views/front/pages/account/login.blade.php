@extends($view_path.'.layouts.master')
@section('content')
<div class="content-push">
	<div class="breadcrumb-box">
        <a href="{{url('/')}}">Home</a>
        <a href="{{url('login')}}">Login</a>
    </div>
    <div class="information-blocks">
        <div class="row">
            <div class="col-sm-6 information-entry">
                <div class="login-box">
                    <div class="article-container style-1">
                        <h3>Registered Customers</h3>
                        <p>Lorem ipsum dolor amet, conse adipiscing, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <form method="post" action="{{url('do-login')}}">
                        @include('front.includes.errors')
                        <label>Email Address</label>
                        <input class="simple-field" type="email" placeholder="Enter Email Address" name="email" value="{{old('email')}}" required />
                        <div class="error-text text-danger">{{$errors->login->first('email')}}</div>
                        <label>Password</label>
                        <input class="simple-field" type="password" placeholder="Enter Password" name="password" required />
                        <div class="error-text text-danger">{{$errors->login->first('password')}}</div>
                        <button type="submit" class="button style-10">Login</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-6 information-entry">
                <div class="login-box">
                    <div class="article-container style-1">
                        <h3>New Customers</h3>
                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                    </div>
                    <form method="post" action="{{url('do-register')}}">
                        @include('front.includes.errors')
                        <label>Email Address</label>
                        <input class="simple-field" type="email" placeholder="Enter Email Address" name="email" value="{{old('email')}}" required/>
                        <div class="error-text text-danger">{{$errors->register->first('email')}}</div>
                        <label>Name</label>
                        <input class="simple-field" type="text" placeholder="Enter Name" name="name" value="{{old('name')}}" required/>
                        <div class="error-text text-danger">{{$errors->register->first('name')}}</div>
                        <label>Password</label>
                        <input class="simple-field" type="password" placeholder="Enter Password" name="password" required/>
                        <div class="error-text text-danger">{{$errors->register->first('password')}}</div>
                        <label>Password Confirmation</label>
                        <input class="simple-field" type="password" placeholder="Enter Password" name="password_confirmation" required/>
                        <div class="error-text text-danger">{{$errors->register->first('password_confirmation')}}</div>
                        <button type="submit" class="button style-10">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
@endsection