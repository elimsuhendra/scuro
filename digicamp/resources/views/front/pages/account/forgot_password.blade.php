@extends($view_path.'.layouts.master')
@section('content')
<div class="row con_checkout">
	<div class="container cus_container">
		<div class="col-md-12 col-sm-12 col-xs-12">
			@if (count($errors) > 0)
			<div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="alert alert-danger alert-dismissable">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			</div>
			@endif
			
			@if($status == "found")
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1 class="center" style="color:#683B11;font-size:20px;margin-bottom:20px;"><b>New Password</b></h1>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12" style="color:#666666;">
				<div class="row">
					<form class="form-horizontal" role="form" method="POST" action="testing">
					{{csrf_field()}}

					<div class="form-group">
						<label class="col-md-4 col-sm-4 control-label">Password :</label>
						<div class="col-md-6 col-sm-6">
							<input type="password" class="form-control" name="password">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 col-sm-4 control-label">Confirm Password :</label>
						<div class="col-md-6 col-sm-6">
							<input type="password" class="form-control" name="password_confirmation">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-4" style="text-align:right;">
							<button type="submit" style="width:50%;padding:5px 0;margin-bottom: 10px;background-color:#666666;color:white;font-family:open_sans_bold !important;border:none!important;">
								Submit
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
			@else
			<div class="col-md-12 col-sm-12 col-xs-12">	
				<h1>Sorry Your Request Goes Wrong Or Token Already Been Used, Please Forgot Password again or Contact Our Team in wakuindonesia@gmail.com</h1>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection