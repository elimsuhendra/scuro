<h3 class="cart-column-title size-1">Detail Shopping</h3>
<div class="side-cart">
    @foreach($cart as $q => $c)
        <div class="traditional-cart-entry style-1">
            <a href="{{url('product')}}/{{$c['data']['name_alias']}}" class="image"><img src="{{asset('components/front/images')}}/product/{{$c['data']['id']}}/{{$c['data']['images']}}" alt="" /></a>
            <div class="content">
                <div class="cell-view">
                    <a href="{{url('product')}}/{{$c['data']['name_alias']}}" class="title">{{$c['data']['name']}}</a>
                    <div class="qty-price">
                        <div class="current">IDR {{number_format($c['data']['price']),0,',','.'}}</div>
                    </div>
                    <div class="quantity-selector">
                        <div class="detail-info-entry-title">Quantity : {{$c['qty']}}</div>
                    </div>
                    <div class="price">
                        <div class="current">IDR {{number_format($c['data']['sub_total']),0,',','.'}}</div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="col-md-12 side-cart-summary">
    <div class="row side-cart-holder">
        <div class="col-md-4">Sub Total</div>
        <div class="col-md-8">IDR {{number_format($checkout_data['sub_total'])}}</div>
    </div>
    <div class="row side-cart-holder">
        <div class="col-md-4">Notes</div>
        <div class="col-md-8">
            <textarea class="simple-field size-1 checkout-notes" name="checkout-notes" disabled>{!!$checkout_data['notes'] ?? ''!!}</textarea>
        </div>
    </div>
     <div class="row side-cart-holder">
        <div class="col-md-4 total-price-label"><strong>Total</strong></div>
        <div class="col-md-8 total-price"><strong>IDR {{number_format($checkout_data['total'])}}</strong></div>
    </div>
</div>

<div class="additional-data">
    <a class="button style-17" href="{{url('cart')}}">Edit Cart <i class="fa fa-edit"></i></a>
</div>