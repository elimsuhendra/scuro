@extends($view_path.'.layouts.shop_master')
@section('content_shop')
<div class="content-push">
  <div class="information-blocks">
  	<div class="loader-container cart-loader">
        <div id="floatingCirclesG">
        <div class="f_circleG" id="frotateG_01"></div>
        <div class="f_circleG" id="frotateG_02"></div>
        <div class="f_circleG" id="frotateG_03"></div>
        <div class="f_circleG" id="frotateG_04"></div>
        <div class="f_circleG" id="frotateG_05"></div>
        <div class="f_circleG" id="frotateG_06"></div>
        <div class="f_circleG" id="frotateG_07"></div>
        <div class="f_circleG" id="frotateG_08"></div>
      </div>
        </div>

        <div class="loader-custom">
      <div id="floatingCirclesH">
        <div class="f_circleH" id="frotateH_01"></div>
        <div class="f_circleH" id="frotateH_02"></div>
        <div class="f_circleH" id="frotateH_03"></div>
        <div class="f_circleH" id="frotateH_04"></div>
        <div class="f_circleH" id="frotateH_05"></div>
        <div class="f_circleH" id="frotateH_06"></div>
        <div class="f_circleH" id="frotateH_07"></div>
        <div class="f_circleH" id="frotateH_08"></div>
      </div>
    </div>
      
    <div class="cart-parts"></div>

  	<div class="row cart_rekomen">
      <div class="cus_container">
      @if($goes_well != "")
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
                  <img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="shop_title3">
                  <div class="shop_bn_line"></div>
                  <span>PRODUK REKOMENDASI</span>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12 sh_bs_2">
            <div class="owl-carousel owl-theme owl_custom">
              @foreach($goes_well as $goes)
              <div class="item">
                  <input type="hidden" data-value="{{ $goes->weight }}" class="weight_{{ $goes->id }}" />

                <div class="col-md-12 col-sm-12 col-xs-12 sh_bs_2_cons">
                  @if(count($discount) > 0)
                    @foreach($discount as $dsc)
                      @if($dsc['product_id'] == $goes->id)
                      <div class="sh_dc_abso_1">
                        <img src="{{ url('components/front/images/icon/disc_float.png') }}" class="img-responsive img_center" />

                        <p class="center">{{ number_format($dsc['discount_amount']),0,',','.' }} %</p>
                      </div>
                      @endif
                    @endforeach
                  @endif

                  <div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12">
                         <img src="{{ asset('components/front/images/product') }}/{{$goes->id}}/{{$goes->image}}" class="img-responsive img_center">

                   <p class="sh_dc_abso_1_1 center"><a href="{{ url('shop/product') }}/{{$goes->name_alias}}">{{ $goes->name }}</a></p>
                   
                   @php
                    $prc = 0;
                    $disc_res = '';
                   @endphp

                    @if(count($discount) > 0)
                      @foreach($discount as $dsc)
                        @if($dsc['product_id'] == $goes->id)
                            @php
                          $disc = $dsc['discount_amount'] / 100;

                          $disc_amount = $goes->price * $disc;

                          $disc_res = $goes->price - $disc_amount;

                          $prc = 1;
                        @endphp
                      @endif
                    @endforeach
                  @endif
                </div>

                @if($disc_res != "")
                  <p class="sh_dc_abso_1_2 center">Rp {{ number_format($disc_res),0,',','.' }}</p>
                @else
                  <p class="sh_dc_abso_1_2_rc center">Rp {{ number_format($goes->price),0,',','.' }}</p>
                @endif

                @if($prc == 1)
                  <p class="sh_dc_abso_1_3 center">Rp {{ number_format($goes->price),0,',','.' }}</p>
                @endif

                <div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-4 col-xs-4">
                  @foreach($attr as $at)
                    @if($at->id == $goes->id)
                      @foreach($at->product_data_attribute_master as $ms)
                        @foreach($ms->product_data_attribute as $pdt)
                          <input type="hidden" class="product_attr_{{ $goes->id }}" data-value="{{ $pdt->product_attribute_data->id }}">
                        @endforeach
                      @endforeach
                    @endif
                  @endforeach
                  <button class="sh_dc_abso_btn add-cart-front" data-id="{{ $goes->id }}">
                    <img src="{{ url('components/front/images/icon/cart4.jpg') }}" class="img-responsive img_center" />
                  </button>
                </div>
                  </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      @endif 
      </div>
    </div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
    $('.owl-carousel').owlCarousel({
	    margin:0,
	    nav:false,
	    dots:false,
	    autoplay:true,
	    autoplayTimeout:1000,
	    autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	})
</script>
@endpush