@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

<div class="row orc_detail">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 center orc_1">
				<h1>{{ $con1->post_name }}</h1>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
			   	<div class="row">
			   		<div class="col-md-6 col-sm-6 col-xs-12">
						<img src="{{ asset('components/front/images/post') }}/{{ $con1->id }}/{{ $con1->thumbnail }}" onerror="this.src='{{ asset('components/both/images/not_found/our-recipes-not-found.png') }}';" class="img-responsive img_center" />
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 orc_des">
								{!! $con1->content !!}
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 dor_btn">
								<a href="{{ url('/recipes/pdf') }}/{{ $con1->id }}"><button class="btn btn-default">Save to PDF</button></a>
								<a href="{{ url('/') }}/{{ Request::segment(1) }}"><button class="btn btn-default">Back</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			
		</div>
	</div>
</div>
@endsection