@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

<div class="row oc_con2">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_tabs2">
		<ul class="nav nav-pills nav-justified">
		    @foreach($con1 as $q => $content1)
			    <li class="{{ $loop->first == true ? 'active' : ''}}"><a class="mh_tabs" data-toggle="pill" href="#menu{{ $q }}">{{ $content1->category_name }}</a></li>
		    @endforeach
		</ul>
		</div>
	</div>
  	
  	<div class="col-md-12 col-sm-12 col-xs-12">
  		<div class="row cus_container2 prod_con">
	  		<div class="tab-content">
	  			@foreach($con1 as $k => $content1)
	  			<div id="menu{{ $k }}" class="tab-pane fade {{ $loop->first == true ? 'in active' : '' }}">
	  			  	<div class="row">
	  			  	   	<div class="container">
	  			  	   		<div class="row">
	  							<div class="col-md-12 col-sm-12 col-xs-12">
	  								{!! $content1->description !!}
	  							</div>

	  							@php 
	  								$get_csm = 0;
	  							@endphp

	  							@foreach($con2 as $content2)
	  								@if($content1->id == $content2->category_id)
	  									@php
	  										$csm = $get_csm++;
	  									@endphp
	  								@endif
		  						@endforeach

		  						@if($get_csm > 0)
	  							<div class="col-md-12 col-sm-12 col-xs-12 center prod_1">
	  								<h1>OUR CONSUMAMBLE PRODUCTS ARE :</h1>
	  							</div>
	  							@endif

	  							<div class="col-md-12 col-sm-12 col-xs-12">
	  								@foreach($con2 as $content2)
		  								@if($content1->id == $content2->category_id)
			  								<div class="row flex_table prod_3">
			  									<div class="col-md-3 col-sm-3 col-xs-12 prod_img">
			  										<img src="{{ asset('components\front\images\post') }}\{{ $content2->id }}\{{ $content2->thumbnail }}" onerror="this.src='{{ asset('components/both/images/not_found/none-background.jpg') }}';" class="img-responsive img_center" />
			  									</div>

			  									<div class="col-md-9 col-sm-9 col-xs-12 prod_2">
		  											<h4>{{ $content2->post_name }}</h4>
		  											{!! $content2->content !!}
			  									</div>
			  								</div>
		  								@endif
	  								@endforeach
	  							</div>
	  						</div>
	  					</div>
	  			 	</div>
	  			</div>
	  			@endforeach
		 	</div>
	 	</div>
  	</div>

  	<div class="col-md-12 col-sm-12 col-xs-12 prod_4">
  		<div class="row prod_5">
  			<div class="col-md-12 col-sm-12 col-xs-12 center prod_6">
  				<h2>OUR PRODUCTS</h2>
  			</div>

  			<div class="col-md-12 col-sm-12 col-xs-12">
  				@foreach($con3 as $content3)
  				<div class="col-md-4 col-sm-4 col-xs-12 prod_7">
  					<div class="col-md-12 col-sm-12 col-xs-12">
  						<img src="{{ asset('components\front\images\post') }}\{{ $content3->id }}\{{ $content3->thumbnail }}" onerror="this.src='{{ asset('components/both/images/not_found/product-not-found.png') }}';" class="img-responsive img_center" />
  					</div>
  				</div>
  				@endforeach
  			</div>
  		</div>
  	</div>
</div>
@endsection

@push('custom_scripts')
<script>
function matchHeight() {
    $('.mh_tabs').matchHeight();
}

$( window ).load(function() {
    matchHeight();
});
</script>
@endpush
