@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

<form action="{{ url('/') }}/{{ Request::segment(1) }}/filter" method="POST">
{{ csrf_field() }}
<div class="row cus_container nw_con1">
	<div class="col-md-12 col-sm-12 col-xs-12 or_3">
		<div class="row flex_table">
			<div class="col-md-4 col-sm-5 col-xs-12">
				<h1>News</h1>
			</div>
			
			<div class="col-md-offset-4 col-md-4 col-sm-offset-1 col-sm-6 col-xs-12">
				<div class="form-group align_right or_con3">
				  <select class="form-control or_1 or_inline" name="pgscuro">
				    <option value="">Page</option>
				    <option value="8">8</option>
				    <option value="16">16</option>
				    <option value="24">24</option>
				    <option value="32">32</option>
				  </select>

				  <button type="submit" class="btn btn-default or_inline or_submit">Submit</button>
				</div>
			</div>
		</div>
	</div>

	@if(count($con1) > 0)
	@foreach($con1 as $content1)
	<div class="col-md-12 col-sm-12 col-xs-12 nw_con1_1">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<img src="{{ asset('components/front/images/post') }}/{{ $content1->id }}/{{ $content1->thumbnail }}" class="img-responsive" />
			</div>

			<div class="col-md-8 col-sm-12 col-xs-12">
				<a href="{{ url('/') }}/{{ Request::segment(1) }}/{{ $content1->post_name_alias }}"><h2>{{ strtoupper($content1->post_name) }}</h2></a><br>
				{!! substr($content1->content, 0, 500) !!} <a href="{{ url('/') }}/{{ Request::segment(1) }}/{{ $content1->post_name_alias }}" class="nw_link"><b>More...</b></a>
			</div>
		</div>
	</div>
	@endforeach

	@if(count($con1) > 0)
	<div class="col-md-12 col-sm-12 col-xs-12 nw_con2 right">
		{{ $con1->links() }}
	</div>
	@endif
	@else
	<div class="col-md-12 col-sm-12 col-xs-12 center">
		<h1>Sorry, No news Found.</h1>
	</div>
	@endif
</div>
</form>
@endsection