@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

<div class="row cus_container dt_nw_con1">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<img src="{{ asset('components/front/images/post') }}/{{ $con1->id }}/{{ $con1->thumbnail }}" class="img-responsive img_center dt_nw_img" />
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>{{ strtoupper($con1->post_name) }}</h1><br>
				{!! $con1->content !!}<br>

				<a href="{{ url('/') }}/{{ Request::segment(1) }}"><button class="dt_cus_btn">Back</button></a>
			</div>
		</div>
	</div>
</div>
@endsection