@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

<div class="row oc_con2">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_tabs">
		<ul class="nav nav-pills nav-justified">
			@foreach($con2 as $q => $content2)
			    <li class="{{ $loop->first == true ? 'active' : ''}}"><a class="mh_tabs" data-toggle="pill" href="#menu{{ $q }}">{{ $content2->post_name }}</a></li>
		    @endforeach
		</ul>
		</div>
	</div>
  	
  	<div class="col-md-12 col-sm-12 col-xs-12">
  		<div class="row cus_container2 oc_con2_1">
	  		<div class="tab-content">
	  			@foreach($con2 as $k => $content2)
	  			<div id="menu{{ $k }}" class="tab-pane fade {{ $loop->first == true ? 'in active' : '' }}">
	  			  <div class="row flex_table">
	  				<div class="col-md-4 col-sm-4 col-xs-12 oc_con2_1_L align_center">
	  					<h1>{!! $content2->description !!}</h1>
	  				</div>

	  				<div class="col-md-8 col-sm-8 col-xs-12">	
	  					{!! $content2->content !!}
	  				</div>
	  			  </div>
	  			</div>
	  			@endforeach
		 	</div>
	 	</div>
  	</div>
</div>

<div class="row flex_table">
	@php $i = 0;  @endphp

	@foreach($con3 as $content3)
		@php $i++;  @endphp

		@if($i % 2 == 0)
			<div class="col-md-4 col-sm-4 col-xs-12 oc_con3">
				<div class="row flex_table">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<img src="{{ asset('components/front/images/post') }}/{{ $content3->id }}/{{ $content3->thumbnail }}" class="img-responsive" />	
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 oc_col">
						<div class="oc_col_bl">
							{!! substr($content3->content, 0, 400) !!}
						</div>
					</div>
				</div>
			</div>
		@else
			<div class="col-md-4 col-sm-4 col-xs-12 oc_con3">
				<div class="row flex_table">
					<div class="col-md-12 col-sm-12 col-xs-12 oc_col oc_rsv">
						<div class="oc_col_bl">
							{!! substr($content3->content, 0, 400) !!}
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<img src="{{ asset('components/front/images/post') }}/{{ $content3->id }}/{{ $content3->thumbnail }}" class="img-responsive" id={{ $loop->first == true ? 'imageid' : '' }} />	
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
</div>
@endsection

@push('custom_scripts')
<script>
function changeHeight(){
    var img = document.getElementById('imageid'); 
    var height = img.clientHeight;
    $(".oc_col").css("height", height);
}

function matchHeight() {
    $('.mh_tabs').matchHeight();
}

$( window ).load(function() {
    matchHeight();
    changeHeight();
});

$( window ).resize(function() {
    changeHeight();
});
</script>
@endpush
