@extends($view_path.'.layouts.master')
@section('content')
<div class="row" id="cn_first">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

<div class="row cu_con">
    <div class="col-md-12 col-sm-12 col-xs-12 cu_con3_1">
        <div class="row cus_container2 flex_table cu_con3_2">
            <div class="col-md-3 col-sm-3 col-xs-12">
                 <a href="{{ url('/') }}" class="ft_a">
                    <img src="{{ url('/components/both/images/web/') }}/{{ $web_logo }}" class="img-responsive img_center" />
                  </a>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12 cu_con3_5">
               {!! $address !!}
                <p class="cu_con3_p">p  {{ $phone }}</p>
                <p class="cu_con3_p">f  {{ $fax }}</p>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 cu_con3_5">
                <h1 class="cu_con3_3">GET SOCIAL WITH US</h1>
                <div class="row cu_con3_4">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="row">    
                            @foreach($social_media as $sosmed)
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row flex_table">
                                	<div class="col-lg-2 col-md-3 col-sm-3 col-xs-2">
	                                	<a href="{{ $sosmed->url }}" target="_blank" class="ft_sosmed"><i class="{{ $sosmed->image }} fa-2x" aria-hidden="true"></i></a>
	                                </div>

	                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-8">
	                                	<a href="{{ $sosmed->url }}" class="ft_sosmed2" target="_blank">{{ $sosmed->socialmedia_name }}</a>
	                                </div>
                            	</div>
                            </div>
                                <!-- <a href="{{ $sosmed->url }}" target="_blank"><i class="{{ $sosmed->image }} fa-2x" aria-hidden="true"></i></a> -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row cu_con2 cus_container2">
	@if (count($errors) > 0)
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="alert alert-danger alert-dismissable">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	</div>
	@endif

	@if(session()->get('success'))
    <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="alert alert-success alert-dismissable">
	     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	        {{ session()->get('success') }}
	    </div>
    </div>
	@endif

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row flex_table">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 cu_con2_1">
						<h2><span><b>LET'S KEEP IN TOUCH</b><span></h2>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3967.0088801578163!2d106.72743521486086!3d-6.12950639556189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d4e01a1c80d%3A0x949d1ad64b177239!2sSedayu+Square!5e0!3m2!1sid!2sid!4v1496805635401" frameborder="0" class="cu_gmaps"allowfullscreen></iframe>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12 cu_con2_3">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h3>Leave us a message</h3>
					</div>
					
					<form action="{{ url('/') }}/{{ Request::segment(1) }}" method="POST">
						{{ csrf_field() }}
						
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
						  		<input type="text" class="form-control" name="co_name" placeholder="Name">
						  	</div>
						</div>	

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
						  		<input type="email" class="form-control" name="co_email" placeholder="Email">
						  	</div>
						</div>		

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
						  		<input type="number" class="form-control" name="co_phone" placeholder="Phone Number">
						  	</div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
						  		<textarea class="form-control" rows="8" name="co_message" placeholder="Message"></textarea>
						  	</div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<div class="col-md-offset-2 col-md-10 col-sm-12 col-xs-12 right">
									<div class="form-group">
										<div class="g-recaptcha cu_recaptcha" data-sitekey="6LeqBR4UAAAAAH5fv6-drwWynsqkolwVR8-XjK2s"></div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<div class="col-md-offset-8 col-md-4 col-sm-offset-8 col-sm-4 col-xs-12 right">
									<div class="form-group">
										<button type="submit" class="cu_btn"><b>SUBMIT</b></button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
function matchHeight() {
    $('.mh_tabs').matchHeight();
}

$(window).load(function() {
    matchHeight();
});
</script>
@endpush
