@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row bn_img">
			<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />

			<div class="bn_con1">
		        <div class="bn_con1_1">
		            <div class="col-md-5 col-sm-7 col-xs-12 bn_con1_2"> 
		                <h1>{{ strtoupper($banner->banner_name) }}</h1>
		                <p>{{ substr($banner->description,0,280) }}</p>
		            </div>
		        </div>
		    </div>
	    </div>
    </div>
</div>

@php $i=1;  @endphp

@foreach($con1 as $content1)
	@php $i++; @endphp

	@if($i % 2 != 0)
	<div class="row bc_con1 cus_container2">
		<div class="col-md-12 col-sm-12 col-xs-12 bc_con1_1">
			<h2>{{ strtoupper($content1->post_name) }}</h2>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-12 bc_con1_2">
					{!! $content1->content !!}
				</div>

				<div class="col-md-offset-2 col-md-5 col-sm-offset-2 col-sm-5 col-xs-12">
					@if($loop->first)
						<p>If you are interested and want to be an agent of Scuro Group or If you are the owner of the store, you can equip your booth with a range of quality products of Scuro Group.  Please contact Us Here</p>
						<a href="{{ url('/') }}/{{ $con2->pages_name_alias }}"><button class="bc_con_btn">HERE</button></a>
					@else 
						<p>For further information, Please contact Us</p>
						<a href="{{ url('/') }}/{{ $con2->pages_name_alias }}"><button class="bc_con_btn">HERE</button></a>
					@endif
				</div>
			</div>
		</div>
	</div>
	@else
	<div class="row bc_con2" style="background-image:url({{ asset('components/front/images/post') }}/{{ $content1->id  }}/{{ $content1->thumbnail  }})">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row bc_con2_1 cus_container2 ">
				<div class="col-md-12 col-sm-12 col-xs-12 bc_con2_2">
					<h2>{{ strtoupper($content1->post_name) }}</h2>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-12 bc_con2_3">
							{!! $content1->content !!}
						</div>

						<div class="col-md-offset-2 col-md-5 col-sm-offset-2 col-sm-5 col-xs-12 bc_con2_4">
							@if($loop->first)
								<p>If you are interested and want to be an agent of Scuro Group or If you are the owner of the store, you can equip your booth with a range of quality products of Scuro Group.  Please contact Us Here</p>
								<a href="{{ url('/') }}/{{ $con2->pages_name_alias }}"><button class="bc_con_btn">HERE</button></a>
							@else 
								<p>For further information, Please contact Us</p>
								<a href="{{ url('/') }}/{{ $con2->pages_name_alias }}"><button class="bc_con_btn">HERE</button></a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
@endforeach
@endsection