@extends($view_path.'.layouts.shop_master')
@section('content_shop')
<div class="row shop_pro_con">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_container">
		    <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12">	
				<div class="swiper-container gallery-top">
			        <div class="swiper-wrapper">
			        	@foreach($product->product_data_images as $pdi)
				            <div class="swiper-slide shop_pro_slide_cus">
				            	<img src="{{ asset('components/front/images/product') }}/{{ $product->id }}/{{ $pdi->images_name }}" class="img-responsive img_center" />
				            </div>
						@endforeach			            
			        </div>
			    </div>

			    <div class="swiper-container gallery-thumbs">
			        <div class="swiper-wrapper">
			        	@foreach($product->product_data_images as $pdi)
				            <div class="swiper-slide shop_pro_slide_cus2">
				            	<img src="{{ asset('components/front/images/product') }}/{{ $product->id }}/{{ $pdi->images_name }}" class="img-responsive img_center" />
				            </div>
			            @endforeach
			        </div>
			    </div>
			</div>

			<div class="col-lg-7 col-md-8 col-sm-8 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 shop_pro_1_cons">
				   	<div class="row flex_table">
						<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
							<p class="shop_pro_1_1">{{ $product->name }}</p>
						</div>

						@if($discount != "")
							@php
								$disc = $discount->amount / 100;

								$disc_amount = $product->price * $disc;

								$disc_res = $product->price - $disc_amount;
							@endphp

							<div class="col-lg-4 col-md-5 col-sm-5 col-xs-6">
								<p class="shop_pro_1_2">Rp. {{number_format($disc_res),0,',','.'}}</p>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
								<p class="shop_pro_1_3">Rp. {{number_format($product->price),0,',','.'}}</p>
							</div>
						@else
							<div class="col-lg-4 col-md-5 col-sm-5 col-xs-6">
								<p class="shop_pro_1_2">Rp. {{number_format($product->price),0,',','.'}}</p>
							</div>
						@endif
					</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 shop_pro_1_desc">
					<p class="shop_pro_1_4">{{ $product->description }}</p>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-offset-7 col-md-5 col-sm-12 col-xs-12">
							<div class="row flex_table">
								<div class="col-lg-7 col-md-8 col-sm-8 col-xs-8 quantity-selector right">
									<input type="hidden" data-value="{{ $product->weight }}" class="weight_{{ $product->id }}" />

									@foreach($product_attribute as $q => $pa)
					                    @foreach($pa['data'] as $pad)
											<input type="hidden" class="entry product-attr {{$loop->first ? 'active' : ''}}" data-id="{{$pad['id']}}" />
					                    @endforeach
					                @endforeach

				                    <input type="text" class="entry number product-qty" value="1" />
									<div class="entry number-minus">&nbsp;</div>
				                    <div class="entry number-plus">&nbsp;</div>
				                </div>

				                <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
				                   <button class="shop_pro_add add-cart btn" data-id="{{$product->id}}">ADD</button>
				                </div>
			                </div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 shop_pro_2">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
							<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="shop_title2">
							<span>DESKRIPSI</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 shop_pro_3">
				{!! $product->content !!}
			</div>
			
			@if($goes_well != "")
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
									<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

								<div class="shop_title3">
									<div class="shop_bn_line"></div>
									<span>PRODUK REKOMENDASI</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 sh_bs_2">
						<div class="owl-carousel owl-theme owl_custom">
							@foreach($goes_well as $goes)
							<div class="item">
		   					  <input type="hidden" data-value="{{ $goes->weight }}" class="weight_{{ $goes->id }}" />

							  <div class="col-md-12 col-sm-12 col-xs-12 sh_bs_2_cons">
							  	@if(count($disco_goes) > 0)
							  		@foreach($disco_goes as $dsc)
							  			@if($dsc['product_id'] == $goes->id)
									  	<div class="sh_dc_abso_1">
									   	 	<img src="{{ url('components/front/images/icon/disc_float.png') }}" class="img-responsive img_center" />

									   	 	<p class="center">{{ number_format($dsc['discount_amount']),0,',','.' }} %</p>
									    </div>
									    @endif
								    @endforeach
							    @endif

						  		<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12">
					               <img src="{{ asset('components/front/images/product') }}/{{$goes->id}}/{{$goes->image}}" class="img-responsive img_center">

								   <p class="sh_dc_abso_1_1 center"><a href="{{ url('shop/product') }}/{{$goes->name_alias}}">{{ $goes->name }}</a></p>
								   
								   @php
								   	$prc = 0;
								   	$disc_res = '';
								   @endphp

								    @if(count($disco_goes) > 0)
								  		@foreach($disco_goes as $dsc)
								  			@if($dsc['product_id'] == $goes->id)
								  			  	@php
													$disc = $dsc['discount_amount'] / 100;

													$disc_amount = $goes->price * $disc;

													$disc_res = $goes->price - $disc_amount;

													$prc = 1;
												@endphp
											@endif
										@endforeach
									@endif
								</div>

								@if($disc_res != "")
									<p class="sh_dc_abso_1_2 center">Rp {{ number_format($disc_res),0,',','.' }}</p>
								@else
									<p class="sh_dc_abso_1_2_rc center">Rp {{ number_format($goes->price),0,',','.' }}</p>
								@endif

								@if($prc == 1)
									<p class="sh_dc_abso_1_3 center">Rp {{ number_format($goes->price),0,',','.' }}</p>
								@endif

								<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-4 col-xs-4">
									@foreach($attr as $at)
										@if($at->id == $goes->id)
											@foreach($at->product_data_attribute_master as $ms)
												@foreach($ms->product_data_attribute as $pdt)
													<input type="hidden" class="product_attr_{{ $goes->id }}" data-value="{{ $pdt->product_attribute_data->id }}">
												@endforeach
											@endforeach
										@endif
									@endforeach
									<button class="sh_dc_abso_btn add-cart-front" data-id="{{ $goes->id }}">
										<img src="{{ url('components/front/images/icon/cart4.jpg') }}" class="img-responsive img_center" />
									</button>
								</div>
						      </div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			@endif 
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
    var galleryTop = new Swiper('.gallery-top', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 10,
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;

    $('.owl-carousel').owlCarousel({
	    margin:0,
	    nav:false,
	    dots:false,
	    autoplay:true,
	    autoplayTimeout:1000,
	    autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	})
</script>
@endpush