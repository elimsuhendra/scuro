@extends($view_path.'.layouts.master')
@section('content')
<div class="row preview__header">
    @include($view_path.'.includes.slide')
</div>

<div class="row hm_con3">
    <div class="col-md-12 col-sm-12 col-xs-12 hm_con3_1">
        <div class="row cus_container2 flex_table hm_con3_2">
            <div class="col-md-3 col-sm-3 col-xs-12">
                 <a href="{{ url('/') }}" class="ft_a">
                    <img src="{{ url('/components/both/images/web/') }}/{{ $web_logo }}" class="img-responsive img_center" />
                  </a>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12 hm_con3_5">
               {!! $address !!}
                <p class="hm_con3_p">p  {{ $phone }}</p>
                <p class="hm_con3_p">f  {{ $fax }}</p>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 hm_con3_5">
                <h1 class="hm_con3_3">GET SOCIAL WITH US</h1>
                <div class="row hm_con3_4">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="row flex_table">    
                            @foreach($social_media as $sosmed)
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <a href="{{ $sosmed->url }}" target="_blank" class="ft_sosmed"><i class="{{ $sosmed->image }} fa-2x" aria-hidden="true"></i></a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('custom_scripts')
	<script>
	    var swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
            effect: 'fade',
            autoplay: 2500,
            autoplayDisableOnInteraction: false,
	    });

        var calcHeight = function() {
            var headerDimensions = $('.preview_header').height();
            $('.full-screen-preview__frame').height($(window).height() - headerDimensions);
          }

          $(document).ready(function() {
            calcHeight();
          });

          $(window).resize(function() {
            calcHeight();
          }).load(function() {
            calcHeight();
          });
    </script>
@endpush