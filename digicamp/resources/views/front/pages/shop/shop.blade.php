@extends($view_path.'.layouts.shop_master')
@section('content_shop')
<div class="row">
	<div class="swiper-container">
	    <div class="swiper-wrapper">
	    	@foreach($slideshow as $slides)
	        <div class="swiper-slide pos_relative cus_swipe_img">
    			<img src="{{ url('components/front/images/slideshow') }}/{{ $slides->image }}" class="img-responsive img_width" />

    			<div class="bg_slide_opacity"></div>

    			<div class="swipe_content">
    				<div class="swipe_content1 left">
    					<h3>{{ $slides->slideshow_name }}</h3>
    					<p>{{ substr($slides->description, 0,615) }}</p>

    					<a href="{{ $slides->url }}"><button class="btn">{{ strtoupper($slides->button_text) }}</button></a>
    				</div>
    			</div>
	        </div>
	        @endforeach
	    </div>

	    <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
	</div>
</div>

<div class="row shop_1">
	<div class="col-md-12 col-sm-12 col-xs-12 center">
		<p>{{ $shop_info }}</p>
	</div>
</div>

<div class="row shop_2">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12 shop_2_ct">
						<div class="row shop_2_ct_col">
							<a href="{{ url('/shop') }}/category/{{ $sticky_category[0]['url'] }}">
									<div class="col-md-12 col-sm-12 col-xs-12 shop_2_ct_img" style="background-image:url('{{ asset('components/front/images/product_category/'.$sticky_category[0]['image']) }}');background-size:cover;background-position:right;">
									<p class="shop_2_ct_tll">{!! str_replace(' ','<br />',$sticky_category[0]['name']) !!}</p>
								</div>
							</a>

							<a href="{{ url('/shop') }}/category/{{ $sticky_category[1]['url'] }}">
								<div class="col-md-12 col-sm-12 col-xs-12 shop_2_ct_img" style="background-image:url('{{ asset('components/front/images/product_category/'.$sticky_category[1]['image']) }}');background-size:cover;background-position:right;">
									<p class="shop_2_ct_tl2l">{!! str_replace(' ','<br />',$sticky_category[1]['name']) !!}</p>
								</div>
							</a>
						</div>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12 shop_2_ct">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
							  <a href="{{ url('shop/flash-deal') }}">
								<img src="{{ url('components/front/images/other/flash_deal.jpg') }}" class="img-responsive img_width" />
							  </a>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12 shop_2_ct">
						<div class="row shop_2_ct_col">
							<a href="{{ url('/shop') }}/category/{{ $sticky_category[2]['url'] }}">
								<div class="col-md-12 col-sm-12 col-xs-12 shop_2_ct_img" style="background-image:url('{{ asset('components/front/images/product_category/'.$sticky_category[2]['image']) }}');background-size:cover;background-position:left;">
									<p class="shop_2_ct_tlr">{!! str_replace(' ','<br />',$sticky_category[2]['name']) !!}</p>
								</div>
							</a>

							<a href="{{ url('/shop') }}/category/{{ $sticky_category[3]['url'] }}">
								<div class="col-md-12 col-sm-12 col-xs-12 shop_2_ct_img" style="background-image:url('{{ asset('components/front/images/product_category/'.$sticky_category[3]['image']) }}');background-size:cover;background-position:left;">
									<p class="shop_2_ct_tl2r">{!! str_replace(' ','<br />',$sticky_category[3]['name']) !!}</p>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					@if($banner != "")
					<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row shop_3">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_container">
			<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_tl">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
							<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="shop_title">
							<div class="shop_bn_line"></div>
							<span>PROMO</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 sh_promo_con">
				@if(count($product_discount) > 0)
				<div class="col-md-6 col-sm-12 col-xs-12 sh_promo_l">
				  <div class="row flex_table">
					<div class="col-md-6 col-sm-6 col-xs-12 sh_pro_1">
					  <div class="row">
					  	<div class="col-md-12 col-sm-12 col-xs-12 sh_pro1_img">
							<a href="{{ url('/shop/product') }}/{{ $product_discount[0]->name_alias }}">
								<img src="{{ url('components/front/images/product') }}/{{ $product_discount[0]->id }}/{{ $product_discount[0]->image }}" class="img-responsive img_center" />
							</a>

							<a href="{{ url('/shop/product') }}/{{ $product_discount[0]->name_alias }}"><h3 class="center sh_pro_1_1">{{ strtoupper($product_discount[0]->name) }}</h3></a>

							
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_1_2">
							<div class="row sh_pro_1_3 flex_table">
								@php
									$disc = $product_discount[0]->amount / 100;

									$disc_amount =  $product_discount[0]->price * $disc;

									$disc_res = $product_discount[0]->price - $disc_amount;
								@endphp

								<div class="col-lg-7 col-md-12 col-sm-7 col-xs-12 center">
									<p class="sh_pro_pr_dc"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
									<p class="sh_pro_pr_rl"><b>Rp {{ number_format($product_discount[0]->price),0,',','.' }}</b></p>
								</div>

								<div class="col-lg-5 col-md-12 col-sm-5 col-xs-12 center sh_dc_pr_con">
									<span class="sh_dc_pr">{{ number_format($product_discount[0]->amount),0,',','.' }} %</span>
								</div>
							</div>
						</div>
					  </div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="row sh_pro_con">
							@for($pr = 1; $pr <= 3; $pr++)
								@if(isset($product_discount[$pr]))
								<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_cus">
								  	<div class="row height_auto flex_table sh_pro_cus2">
										<div class="col-md-3 col-sm-3 col-xs-12">	
										  	<div class="row">
												<a href="{{ url('/shop/product') }}/{{ $product_discount[$pr]->name_alias }}">
													<img src="{{ url('components/front/images/product') }}/{{ $product_discount[$pr]->id }}/{{ $product_discount[$pr]->image }}" class="img-responsive img_center" />
												</a>
											</div>
										</div>

										<div class="col-md-9 col-sm-9 col-xs-12">
										  <div class="row">
									  	  	<div class="col-md-12 col-sm-12 col-xs-12">
										  	  <div class="row">
											  	<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_3 cus_center">
											  	   	<a href="{{ url('/shop/product') }}/{{ $product_discount[$pr]->name_alias }}"><p><b>{{ strtoupper($product_discount[$pr]->name) }}</b></p></a>
											    </div>

											    <div class="col-md-12 col-sm-12 col-xs-12">
													<div class="row sh_pro_2_1 flex_table">

														@php
															$disc = $product_discount[$pr]->amount / 100;

															$disc_amount =  $product_discount[$pr]->price * $disc;

															$disc_res = $product_discount[$pr]->price - $disc_amount;
														@endphp
														<div class="col-lg-6 col-md-12 col-sm-7 col-xs-12 cus_center">
															<p class="sh_pro_pr_dc2"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
															<p class="sh_pro_pr_rl2"><b>Rp {{ number_format($product_discount[$pr]->price),0,',','.' }}</b></p>
														</div>

														<div class="col-lg-6 col-md-12 col-sm-5 col-xs-12 center sh_dc_pr_con2">
															<span class="sh_dc_pr2">{{ number_format($product_discount[$pr]->amount),0,',','.' }} %</span>
														</div>
													</div>
										   		</div>
										   	  </div>
											</div>
										  </div>
										</div>
									</div>
								</div>
								@endif
							@endfor
						</div>
					</div>
				  </div>
				</div>

					@if(isset($product_discount[4]))
					<div class="col-md-6 col-sm-12 col-xs-12 sh_promo_r">
					  <div class="row flex_table">
						<div class="col-md-6 col-sm-6 col-xs-12 sh_pro_1">
						  <div class="row">
						  	<div class="col-md-12 col-sm-12 col-xs-12 sh_pro1_img">
								<img src="{{ url('components/front/images/product') }}/{{ $product_discount[4]->id }}/{{ $product_discount[4]->image }}" class="img-responsive img_center" />

								<a href="{{ url('/shop/product') }}/{{ $product_discount[4]->name_alias }}"><h3 class="center sh_pro_1_1">{{ strtoupper($product_discount[4]->name) }}</h3></a>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_1_2">
								<div class="row sh_pro_1_3 flex_table">
									@php
										$disc = $product_discount[4]->amount / 100;

										$disc_amount =  $product_discount[4]->price * $disc;

										$disc_res = $product_discount[4]->price - $disc_amount;
									@endphp

									<div class="col-lg-7 col-md-12 col-sm-7 col-xs-12 center">
										<p class="sh_pro_pr_dc"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
										<p class="sh_pro_pr_rl"><b>Rp {{ number_format($product_discount[4]->price),0,',','.' }}</b></p>
									</div>

									<div class="col-lg-5 col-md-12 col-sm-5 col-xs-12 center sh_dc_pr_con">
										<span class="sh_dc_pr">{{ number_format($product_discount[4]->amount),0,',','.' }} %</span>
									</div>
								</div>
							</div>
						  </div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="row sh_pro_con">
								@for($pj = 5; $pj <= 7; $pj++)
									@if(isset($product_discount[$pj]))
									<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_cus">
									  	<div class="row height_auto flex_table sh_pro_cus2">
											<div class="col-md-3 col-sm-3 col-xs-12">	
											  	<div class="row">
													<img src="{{ url('components/front/images/product') }}/{{ $product_discount[$pj]->id }}/{{ $product_discount[$pj]->image }}" class="img-responsive img_center" />
												</div>
											</div>

											<div class="col-md-9 col-sm-9 col-xs-12">
											  <div class="row">
										  	  	<div class="col-md-12 col-sm-12 col-xs-12">
											  	  <div class="row">
												  	<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_3 cus_center">
												  	   	<a href="{{ url('/shop/product') }}/{{ $product_discount[$pj]->name_alias }}"><p><b>{{ strtoupper($product_discount[$pj]->name) }}</b></p></a>
												    </div>

												    <div class="col-md-12 col-sm-12 col-xs-12">
														<div class="row sh_pro_2_1 flex_table">

															@php
																$disc = $product_discount[$pj]->amount / 100;

																$disc_amount =  $product_discount[$pj]->price * $disc;

																$disc_res = $product_discount[$pj]->price - $disc_amount;
															@endphp
															<div class="col-lg-6 col-md-12 col-sm-7 col-xs-12 cus_center">
																<p class="sh_pro_pr_dc2"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
																<p class="sh_pro_pr_rl2"><b>Rp {{ number_format($product_discount[$pj]->price),0,',','.' }}</b></p>
															</div>

															<div class="col-lg-6 col-md-12 col-sm-5 col-xs-12 center sh_dc_pr_con2">
																<span class="sh_dc_pr2">{{ number_format($product_discount[$pj]->amount),0,',','.' }} %</span>
															</div>
														</div>
											   		</div>
											   	  </div>
												</div>
											  </div>
											</div>
										</div>
									</div>
									@endif
								@endfor
							</div>
						</div>
					  </div>
					</div>
					@endif
				@endif
			</div>
		</div>
	</div>
</div>

<div class="row shop_4">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_container">
			<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_tl">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
							<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="shop_title">
							<div class="shop_bn_line"></div>
							<span>PRODUK TERBARU</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 sh_bs_2">
				<div class="owl-carousel owl-theme owl_custom flex_table">
					@foreach($product_terbaru as $fdt)
					<div class="item">
					  <input type="hidden" data-value="{{ $fdt->weight }}" class="weight_{{ $fdt->id }}" />

					  @foreach($attr as $at)
						@if($at->id == $fdt->id)
							@foreach($at->product_data_attribute_master as $ms)
								@foreach($ms->product_data_attribute as $pdt)
									<input type="hidden" class="product_attr_{{ $fdt->id }}" data-value="{{ $pdt->product_attribute_data->id }}" />
								@endforeach
							@endforeach
						@endif
					  @endforeach

					  <div class="col-md-12 col-sm-12 col-xs-12 sh_bs_2_cons">
					  	@if(count($disc_trb) > 0)
						  	@foreach($disc_trb as $dc_trb)
							  	@if($dc_trb['product_id'] == $fdt->id)
							  	<div class="sh_dc_abso_1">
							   	 	<img src="{{ url('components/front/images/icon/disc_float.png') }}" class="img-responsive img_center" />

							   	 	<p class="center">{{ number_format($dc_trb['amount']),0,',','.' }} %</p>
							    </div>
							    @endif
						    @endforeach
					    @endif

				  		<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12">
						   <img src="{{ asset('components/front/images/product') }}/{{$fdt->id}}/{{$fdt->image}}" class="img-responsive img_width">

						   <p class="sh_dc_abso_1_1 center"><a href="{{ url('shop/product') }}/{{ $fdt->name_alias }}">{{ strtoupper($fdt->name) }}</a></p>

						   @php
							   	$prc = 0;
							   	$disc_res = '';
						    @endphp

						    @if(count($disc_trb) > 0)
								@foreach($disc_trb as $dc_trb)
									@if($dc_trb['product_id'] == $fdt->id)
										@php
											$disc = $dc_trb['amount'] / 100;

											$disc_amount = $fdt->price * $disc;

											$disc_res = $fdt->price - $disc_amount;

											$prc = 1;
										@endphp
									@endif
								@endforeach
							@endif

							@if($disc_res != "")
								<p class="sh_dc_abso_1_2 center">Rp. {{number_format($disc_res),0,',','.'}}</p>
							@else
							   <p class="sh_dc_abso_1_2 center">Rp. {{number_format($fdt->price),0,',','.'}}</p>
							@endif

						   @if($prc == 1)
						   	<p class="sh_dc_abso_1_3 center">Rp {{number_format($fdt->price),0,',','.'}}</p>
						   @else
						   <p>&nbsp;</p>
						   @endif
						</div>

						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-4 col-xs-4">
							<button class="sh_dc_abso_btn add-cart-front" data-id="{{ $fdt->id }}">
								<img src="{{ url('components/front/images/icon/cart4.jpg') }}" class="img-responsive img_center" />
							</button>
						</div>
				      </div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row shop_5">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_container">
			<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_tl">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
							<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="shop_title">
							<div class="shop_bn_line"></div>
							<span>BEST SELLER</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 flex_table">
				@if(count($product_terlaris) > 0)
				<div class="col-md-4 col-sm-4 col-xs-12 shop_5_l">
					<input type="hidden" data-value="{{ $product_terlaris[0]->weight }}" class="weight_{{ $product_terlaris[0]->id }}" />

					@if(count($disc_trl) > 0)
					<div class="col-md-12 col-sm-12 col-xs-12 shop_5_disc">
				  		<div class="shop_5_disc1">
					  		<p>{{ number_format($disc_trl[0]['amount']),0,',','.' }} %</p>
					  	</div>
				  	</div>
				  	@endif

				  <div class="row shop_5_lrow">
				    <div class="col-md-12 col-sm-12 col-xs-12">
				      <div class="row">
					  	<div class="col-md-12 col-sm-12 col-xs-12">
					  	  	<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12">
					  	  		<img src="{{ url('components/front/images/product') }}/{{ $product_terlaris[0]->id }}/{{ $product_terlaris[0]->image }}" class="img-responsive img_center" />
					  	  	</div>
					  	</div>

					  	<div class="col-md-12 col-sm-12 col-xs-12 shop_5_lname">
					  	  <div class="row">
					  		<a href="{{ url('shop/product') }}/{{ $product_terlaris[0]->name_alias }}"><p>{{ strtoupper($product_terlaris[0]->name) }}</p></a>
					  	  </div>
					  	</div>

					  	@php
						   	$prc = 0;
						   	$disc_res = '';
					    @endphp

					    @if(count($disc_trl) > 0)
							@foreach($disc_trl as $dc_trb)
								@if($dc_trb['product_id'] == $product_terlaris[0]->id)
									@php
										$disc = $dc_trb['amount'] / 100;

										$disc_amount = $product_terlaris[0]->price * $disc;

										$disc_res = $product_terlaris[0]->price - $disc_amount;

										$prc = 1;
									@endphp
								@endif
							@endforeach
						@endif

					  	<div class="col-md-12 col-sm-12 col-xs-12 shop_5_ldes">
					  	  <div class="row flex_table">
					  		<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 center">
					  			@if($disc_res != "")
									<p class="shop5_pro_pr_dc"><b>Rp {{number_format($disc_res),0,',','.'}}</b></p>
								@else
									<p class="shop5_pro_pr_dc2"><b>Rp {{number_format($product_terlaris[0]->price),0,',','.'}}</b></p>
								@endif

						       @if($prc == 1)
						       		<p class="shop5_pro_pr_rl"><b>Rp {{number_format($product_terlaris[0]->price),0,',','.'}}</b></p>
						       @endif
							</div>

							<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 center sh_dc_pr_con">
								@foreach($attr as $at)
									@if($at->id == $product_terlaris[0]->id)
										@foreach($at->product_data_attribute_master as $ms)
											@foreach($ms->product_data_attribute as $pdt)
												<input type="hidden" class="product_attr_{{ $product_terlaris[0]->id }}" data-value="{{ $pdt->product_attribute_data->id }}" />
											@endforeach
										@endforeach
									@endif
								  @endforeach


								<button class="sh_dc_abso_btn add-cart-front" data-id="{{ $product_terlaris[0]->id }}">
									<img src="{{ url('components/front/images/icon/cart3.jpg') }}" class="img-responsive img_center" />
								</button>
							</div>
						  </div>
					  	</div>
					  </div>
					</div>
				  </div>
				</div>

				@if(count($product_terlaris) > 1)
				<div class="col-md-8 col-sm-8 col-xs-12 shop5_r1_cons">
				  <div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12 shop_5_r1">
					  <div class="row">
					  	@for($lk=1; $lk<=2; $lk++)
						  	@if(isset($product_terlaris[$lk]))
							<div class="col-md-12 col-sm-12 col-xs-12 shop_5_r1_1">
								<div class="row">
									@if(count($disc_trl) > 0)
										@foreach($disc_trl as $dc_trl)
										  	@if($dc_trl['product_id'] == $product_terlaris[$lk]->id)
											<div class="col-md-12 col-sm-12 col-xs-12 shop_5_disc">
										  		<div class="shop_5_disc1">
											  		<p>{{ number_format($dc_trl['amount']),0,',','.' }} }} %</p>
											  	</div>
										  	</div>
										  	@endif
										@endforeach
								  	@endif

								  	<div class="col-md-12 col-sm-12 col-xs-12">
								  		<div class="row">
								  			<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12 shop_5_rimg">
								  				<img src="{{ url('components/front/images/product') }}/{{ $product_terlaris[$lk]->id }}/{{ $product_terlaris[$lk]->image }}" class="img-responsive img_center" />
								  			</div>
								  		</div>
								  	</div>

								  	<div class="col-md-12 col-sm-12 col-xs-12">
								  	  <a href="{{ url('/shop/product') }}/{{ $product_terlaris[$lk]->name_alias }}">	
								  		<p class="shop_5_rtitle center">{{ strtoupper($product_terlaris[$lk]->name) }}</p>
								  	  </a>
								  	</div>

								  	<div class="col-md-12 col-sm-12 col-xs-12 shop_5_rprice">
								  		<div class="row flex_table">
								  			@php
											   	$prc = 0;
											   	$disc_res = '';
										    @endphp

										    @if(count($disc_trl) > 0)
												@foreach($disc_trl as $dc_trl)
													@if($dc_trl['product_id'] == $product_terlaris[$lk]->id)
														@php
															$disc = $dc_trl['amount'] / 100;

															$disc_amount = $product_terlaris[$lk]->price * $disc;

															$disc_res = $product_terlaris[$lk]->price - $disc_amount;

															$prc = 1;
														@endphp
													@endif
												@endforeach
											@endif


								  			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 center align_center">
												@if($disc_res != "")
													<p class="shop5_rpro_pr_rl"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
												@else
													<p class="shop5_rpro_pr_rl"><b>Rp {{ number_format($product_terlaris[$lk]->price),0,',','.' }}</b></p>
												@endif

												@if($prc == 1)
													<p class="shop5_rpro_pr_dc"><b>Rp {{ number_format($product_terlaris[$lk]->price),0,',','.' }}</b></p>
												@endif
											</div>

											<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 center sh_dc_pr_con shop_5_nopad">
											  <div class="row">	
											  	@foreach($attr as $at)
													@if($at->id == $product_terlaris[$lk]->id)
														@foreach($at->product_data_attribute_master as $ms)
															@foreach($ms->product_data_attribute as $pdt)
																<input type="hidden" class="product_attr_{{ $product_terlaris[$lk]->id }}" data-value="{{ $pdt->product_attribute_data->id }}" />
															@endforeach
														@endforeach
													@endif
												  @endforeach

												<button class="sh_dc_abso_btn shop5_nobut add-cart-front" data-id="{{ $product_terlaris[$lk]->id }}">
													<img src="{{ url('components/front/images/icon/cart3.jpg') }}" class="img-responsive img_center img_width" />
												</button>
											  </div>
											</div>
								  		</div>
								  	</div>
								</div>
							</div>
							@endif
						@endfor
					  </div>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12 shop_5_r1">
					  <div class="row">
					  	@for($lk2=3; $lk2<=4; $lk2++)
						  	@if(isset($product_terlaris[$lk2]))
							<div class="col-md-12 col-sm-12 col-xs-12 shop_5_r1_1">
								<div class="row">
									@if(count($disc_trl) > 0)
										@foreach($disc_trl as $dc_trl)
										  	@if($dc_trl['product_id'] == $product_terlaris[$lk2]->id)
											<div class="col-md-12 col-sm-12 col-xs-12 shop_5_disc">
										  		<div class="shop_5_disc1">
											  		<p>{{ number_format($dc_trl['amount']),0,',','.' }} }} %</p>
											  	</div>
										  	</div>
										  	@endif
										@endforeach
								  	@endif

								  	<div class="col-md-12 col-sm-12 col-xs-12">
								  		<div class="row">
								  			<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12 shop_5_rimg">
								  				<img src="{{ url('components/front/images/product') }}/{{ $product_terlaris[$lk2]->id }}/{{ $product_terlaris[$lk2]->image }}" class="img-responsive img_center" />
								  			</div>
								  		</div>
								  	</div>

								  	<div class="col-md-12 col-sm-12 col-xs-12">
								  	  <a href="{{ url('/shop/product') }}/{{ $product_terlaris[$lk2]->name_alias }}">	
								  		<p class="shop_5_rtitle center">{{ strtoupper($product_terlaris[$lk2]->name) }}</p>
								  	  </a>
								  	</div>

								  	<div class="col-md-12 col-sm-12 col-xs-12 shop_5_rprice">
								  		<div class="row flex_table">
								  			@php
											   	$prc = 0;
											   	$disc_res = '';
										    @endphp

										    @if(count($disc_trl) > 0)
												@foreach($disc_trl as $dc_trl)
													@if($dc_trl['product_id'] == $product_terlaris[$lk2]->id)
														@php
															$disc = $dc_trl['amount'] / 100;

															$disc_amount = $product_terlaris[$lk2]->price * $disc;

															$disc_res = $product_terlaris[$lk2]->price - $disc_amount;

															$prc = 1;
														@endphp
													@endif
												@endforeach
											@endif


								  			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 center align_center">
												@if($disc_res != "")
													<p class="shop5_rpro_pr_rl"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
												@else
													<p class="shop5_rpro_pr_rl"><b>Rp {{ number_format($product_terlaris[$lk2]->price),0,',','.' }}</b></p>
												@endif

												@if($prc == 1)
													<p class="shop5_rpro_pr_dc"><b>Rp {{ number_format($product_terlaris[$lk2]->price),0,',','.' }}</b></p>
												@endif
											</div>

											<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 center sh_dc_pr_con shop_5_nopad">
											  <div class="row">	
											  	@foreach($attr as $at)
													@if($at->id == $product_terlaris[$lk2]->id)
														@foreach($at->product_data_attribute_master as $ms)
															@foreach($ms->product_data_attribute as $pdt)
																<input type="hidden" class="product_attr_{{ $product_terlaris[$lk2]->id }}" data-value="{{ $pdt->product_attribute_data->id }}" />
															@endforeach
														@endforeach
													@endif
												  @endforeach

												<button class="sh_dc_abso_btn shop5_nobut add-cart-front" data-id="{{ $product_terlaris[$lk2]->id }}">
													<img src="{{ url('components/front/images/icon/cart3.jpg') }}" class="img-responsive img_center img_width" />
												</button>
											  </div>
											</div>
								  		</div>
								  	</div>
								</div>
							</div>
							@endif
						@endfor
					  </div>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12 shop_5_r1">
					  <div class="row">
					  	@for($lk3=5; $lk3<=6; $lk3++)
						  	@if(isset($product_terlaris[$lk3]))
							<div class="col-md-12 col-sm-12 col-xs-12 shop_5_r1_1">
								<div class="row">
									@if(count($disc_trl) > 0)
										@foreach($disc_trl as $dc_trl)
										  	@if($dc_trl['product_id'] == $product_terlaris[$lk3]->id)
											<div class="col-md-12 col-sm-12 col-xs-12 shop_5_disc">
										  		<div class="shop_5_disc1">
											  		<p>{{ number_format($dc_trl['amount']),0,',','.' }} }} %</p>
											  	</div>
										  	</div>
										  	@endif
										@endforeach
								  	@endif

								  	<div class="col-md-12 col-sm-12 col-xs-12">
								  		<div class="row">
								  			<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12 shop_5_rimg">
								  				<img src="{{ url('components/front/images/product') }}/{{ $product_terlaris[$lk3]->id }}/{{ $product_terlaris[$lk3]->image }}" class="img-responsive img_center" />
								  			</div>
								  		</div>
								  	</div>

								  	<div class="col-md-12 col-sm-12 col-xs-12">
								  	  <a href="{{ url('/shop/product') }}/{{ $product_terlaris[$lk3]->name_alias }}">	
								  		<p class="shop_5_rtitle center">{{ strtoupper($product_terlaris[$lk3]->name) }}</p>
								  	  </a>
								  	</div>

								  	<div class="col-md-12 col-sm-12 col-xs-12 shop_5_rprice">
								  		<div class="row flex_table">
								  			@php
											   	$prc = 0;
											   	$disc_res = '';
										    @endphp

										    @if(count($disc_trl) > 0)
												@foreach($disc_trl as $dc_trl)
													@if($dc_trl['product_id'] == $product_terlaris[$lk3]->id)
														@php
															$disc = $dc_trl['amount'] / 100;

															$disc_amount = $product_terlaris[$lk3]->price * $disc;

															$disc_res = $product_terlaris[$lk3]->price - $disc_amount;

															$prc = 1;
														@endphp
													@endif
												@endforeach
											@endif


								  			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 center align_center">
												@if($disc_res != "")
													<p class="shop5_rpro_pr_rl"><b>Rp {{ number_format($disc_res),0,',','.' }}</b></p>
												@else
													<p class="shop5_rpro_pr_rl"><b>Rp {{ number_format($product_terlaris[$lk3]->price),0,',','.' }}</b></p>
												@endif

												@if($prc == 1)
													<p class="shop5_rpro_pr_dc"><b>Rp {{ number_format($product_terlaris[$lk3]->price),0,',','.' }}</b></p>
												@endif
											</div>

											<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 center sh_dc_pr_con shop_5_nopad">
											  <div class="row">	
											  	@foreach($attr as $at)
													@if($at->id == $product_terlaris[$lk3]->id)
														@foreach($at->product_data_attribute_master as $ms)
															@foreach($ms->product_data_attribute as $pdt)
																<input type="hidden" class="product_attr_{{ $product_terlaris[$lk3]->id }}" data-value="{{ $pdt->product_attribute_data->id }}" />
															@endforeach
														@endforeach
													@endif
												  @endforeach

												<button class="sh_dc_abso_btn shop5_nobut add-cart-front" data-id="{{ $product_terlaris[$lk3]->id }}">
													<img src="{{ url('components/front/images/icon/cart3.jpg') }}" class="img-responsive img_center img_width" />
												</button>
											  </div>
											</div>
								  		</div>
								  	</div>
								</div>
							</div>
							@endif
						@endfor
					  </div>
					</div>
				  </div>
				</div>
				@endif
				@endif
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        effect: 'fade',
        autoplay: 2400,
        autoplayDisableOnInteraction: false,
    });

    $('.owl-carousel').owlCarousel({
	    margin:0,
	    nav:true,
	    dots:false,
	    autoplay:true,
	    autoplayTimeout:1000,
	    autoplayHoverPause:true,
	    navText : ["<img src='{{ url('components/front/images/icon/iprev.png') }}' class='img-responsive img_center' />","<img src='{{ url('components/front/images/icon/inext.png') }}' class='img-responsive img_center' />"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	})
</script>
@endpush