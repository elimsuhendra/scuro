@extends($view_path.'.layouts.shop_master')
@section('content_shop')
<div class="row shop_category_banner">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row">
			<img src="{{ url('components/front/images/mockup/banner_category.jpg') }}" class="img-responsive img_center" />
		</div>
	</div>
</div>

<div class="row shop_category_con">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="cus_container">
			<div class="col-md-12 col-sm-12 col-xs-12 sh_pro_tl">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
							<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="shop_title3">
							<div class="shop_bn_line"></div>
							<span>{{ strtoupper($title) }}</span>
						</div>
					</div>
				</div>
			</div>

			@if(count($product) > 0)
			<div class="col-md-12 col-sm-12 col-xs-12 shop_category_cons">
				<div class="row flex_table">
					@foreach($product as $fdt)
			    	<input type="hidden" data-value="{{ $fdt->weight }}" class="weight_{{ $fdt->id }}" />
					
				    	@foreach($attr as $at)
							@if($at->id == $fdt->id)
								@foreach($at->product_data_attribute_master as $ms)
									@foreach($ms->product_data_attribute as $pdt)
										<input type="hidden" class="product_attr_{{ $fdt->id }} hm_con1_9" data-value="{{ $pdt->product_attribute_data->id }}">
									@endforeach
								@endforeach
							@endif
						@endforeach

						<div class="col-md-3 col-sm-6 col-xs-12 shop_category_pro_cons">
							<div class="row">
								@if(count($discount) > 0)
									@foreach($discount as $dcs)
										@if($dcs->product_id == $fdt->id)
											@foreach($fdt->product_data_attribute_master as $pdtm)	
							   					@if($pdtm->stock > 0)
												<div class="col-md-12 col-sm-12 col-xs-12 shop_category_disc">
													<img src="{{ url('components/front/images/other/disc2.png') }}" class="img-responsive" />
													<p>{{ number_format($dcs->amount),0,',','.' }}% OFF</p>
												</div>
												@else
												<div class="col-md-12 col-sm-12 col-xs-12 shop_category_disc">
													<img src="{{ url('components/front/images/other/disc2_off.png') }}" class="img-responsive" />
													<p>{{ number_format($dcs->amount),0,',','.' }}% OFF</p>
												</div>
												@endif
											@endforeach
										@endif
									@endforeach
								@endif

								<div class="col-md-12 col-sm-12 col-xs-12 shop_category_pro">
									<div class="row">
										<div class="col-md-offset-1 col-md-10 col-sm-offset-2 col-sm-8 col-xs-12">
											<img src="{{ url('components/front/images/product') }}/{{ $fdt->id }}/{{ $fdt->image }}" class="img-responsive img_center" />
										</div>
									</div>
								</div>

								<div class="col-md-12 col-sm-12 col-xs-12">
									<p class="shop_category_pro_tl"><a href="{{ url('shop/product') }}/{{$fdt->name_alias}}">{{ $fdt->name }}</a></p>
								</div>

								@php
								   	$prc = 0;
								   	$disc_res = '';
							    @endphp

								@if(count($discount) > 0)
									@foreach($discount as $dcs)
										@if($dcs->product_id == $fdt->id)
											@php
												$disc = $dcs->amount / 100;

												$disc_amount = $fdt->price * $disc;

												$disc_res = $fdt->price - $disc_amount;

												$prc = 1;
											@endphp
										@endif
									@endforeach
								@endif

								@if($disc_res != "")
									<div class="col-md-12 col-sm-12 col-xs-12">
										<p class="shop_category_pro_disc">Rp. {{ number_format($disc_res),0,',','.' }}</p>
									</div>
								@else
									<div class="col-md-12 col-sm-12 col-xs-12">
										<p class="shop_category_pro_disc">Rp. {{ number_format($fdt->price),0,',','.' }}</p>
									</div>
								@endif

								@if($prc == 1)
								<div class="col-md-12 col-sm-12 col-xs-12">
									<p class="shop_category_pro_rl">Rp {{ number_format($fdt->price),0,',','.' }}</p>
								</div>
								@endif
							</div>
						</div>
					@endforeach
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 center cus_pagination">
					{{ $product->links() }}
				</div>
			</div>


			@else
			<div class="col-md-12 col-sm-12 col-xs-12 shop_category_cons_not">
				<div class="row center">
					<h3>Sorry, No Product in this Category.</h3>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection