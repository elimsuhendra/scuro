@extends($view_path.'.layouts.shop_master')
@section('content_shop')
<div class="row flash_banner">
	<img src="{{ url('components/front/images/banner') }}/{{ $banner->image }}" class="img-responsive img_width" />
</div>

<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 flash_pro_con">
				<div class="row">
				@foreach($pro_flash as $k => $pr_f)
					<div class="col-md-6 col-sm-6 col-xs-12 flash_pro">
						<div class="row flash_pro2">
							<div class="col-md-12 col-sm-12 col-xs-12 flash_disc">
								@if($pr_f['product_stock'] >= $pr_f['flash_stock'] && $pr_f['flash_stock'] > 0)
									<img src="{{ url('components/front/images/other/disc3.png') }}" class="img-responsive" />
								@else
									<img src="{{ url('components/front/images/other/disc4.png') }}" class="img-responsive" />
								@endif

								<input type="hidden" id="back_{{ $k }}" data-value="{{ $pr_f['flash_id'] }}" />

								<input type="hidden" data-value="{{ $pr_f['weight'] }}" class="weight_{{ $pr_f['product_id'] }}" />
								
								<p class="flash_disc_off">{{ $pr_f['discount'] }}% OFF</p>
								
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row flex_table flash_img">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="row flash_pr_img_2">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<img src="{{ url('components/front/images/product') }}/{{ $pr_f['product_id'] }}/{{ $pr_f['image'] }}" class="img-responsive img_center" />
											</div>
										</div>
									</div>

									<div class="col-md-6 col-sm-12 col-xs-12">
										@php
											$disc = $pr_f['discount'] / 100;

											$disc_amount = $pr_f['price'] * $disc;

											$disc_res = $pr_f['price'] - $disc_amount;
										@endphp

										<div class="col-md-12 col-sm-12 col-xs-12 flash_price">
											<p class="flash_pr_n">Harga Normal</p>
											<p class="flash_pr_nor">Rp {{ number_format($pr_f['price']),0,',','.' }}</p>
										</div>

										<div class="col-md-12 col-sm-12 col-xs-12 flash_pr_disc">
											<p class="flash_dc_n">Anda Hemat</p>
											<p class="flash_dc_nor">Rp {{ number_format($disc_amount),0,',','.' }}</p>
										</div>

										<div class="col-md-12 col-sm-12 col-xs-12 flash_pr_disc2">
											<p class="flash_pr_dic2">Rp {{ number_format($disc_res),0,',','.' }}</p>
										</div>
									</div>
								</div>
							</div>

							@php
								$con_from = date_create($pr_f['valid_from']); 
								$con_to = date_create($pr_f['valid_to']); 

								$date_now = date('H:i:s');
								$date_nows = date('m d, Y');
								$dt_from = date_format($con_from,"H:i:s");
								$dt_to = date_format($con_to,"H:i:s");

								if($date_now < $dt_from){
									$des_status = 0;
									$des_text 	= "COMING SOON";
									$des_text2 	= "Deal Dimulai";
									$dt 		= $date_nows.' '.$dt_from;
								} else if($date_now >= $dt_from && $date_now <= $dt_to){
									$des_status = 1;
									$dt 		= $date_nows.' '.$dt_to;
									$des_text   = "Add To Cart";
									$des_text2 	= "Deal Berakhir";
								} else if($date_now > $dt_to){
									$des_status = 2;
									$des_text   = "DEAL BERAKHIR";
									$des_text2   = "";
									$dt  	    = '';
								} 
							@endphp


							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									@if($des_status == 0)
										<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12 flash_coming_con">
											<p>{{ $des_text }}</p>
										</div>
									@elseif($des_status == 1)
										@if($pr_f['product_stock'] >= $pr_f['flash_stock'] && $pr_f['flash_stock'] > 0)
											<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12 flash_coming_con {{ $des_status == 1 ? '' : 'flash_hidden' }}">
												<input type="hidden" class="product_attr_{{ $pr_f['product_id'] }}" data-value="{{ $pr_f['product_atr_data_id'] }}" />

												<input type="hidden" class="flash_{{ $pr_f['product_id'] }}" data-flash="{{ $pr_f['flash_id'] }}" data-id="{{ $pr_f['product_id'] }}" />

												<button class="sh_dc_abso_btn add-cart-front img_width" data-id="{{ $pr_f['product_id'] }}">
													<p class="center">{{ $des_text }}</p>
												</button>
											</div>
										@else
											<div class="col-md-12 col-sm-12 col-xs-12 center {{ $des_status == 1 ? '' : 'flash_hidden' }}">
												<p class="flash_next_date_stock">STOCK HABIS</p>
											</div>
										@endif
									@elseif($des_status == 2)
										<div class="col-md-12 col-sm-12 col-xs-12 center }}">
											<p class="flash_next_date_stock">{{ $des_text }}</p>
										</div>
									@endif
								</div>	
							</div>

							@if($des_status == 0 || $des_status == 1)
								<div class="col-md-12 col-sm-12 col-xs-12 flash_countdown">
									<p class="time" id="count_{{ $k }}" data-id="{{ $k }}" data-text="{{ $des_text2 }}" data-time="{{ $dt }}"></p>
								</div>
							@endif
						</div>
					</div>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row cus_container">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
							<img src="{{ url('components/front/images/other/home-icon-tl.png') }}" class="img-responsive img_center" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="shop_title3">
							<div class="shop_bn_line"></div>
							<span>FLASH SALE SELANJUTNYA</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 flash_next_con">
				<div class="row flex_table">
				@if(count($next_flash) > 0)
					@foreach($next_flash as $nx_flash)
					<div class="col-md-3 col-sm-6 col-xs-12 flash_next_pro">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 flash_next_disc">
							@if($nx_flash['product_stock'] >= $nx_flash['flash_stock'])
								<img src="{{ url('components/front/images/other/disc3.png') }}" class="img-responsive" />
							@else
								<img src="{{ url('components/front/images/other/disc4.png') }}" class="img-responsive" />
							@endif

							<p>{{ $nx_flash['discount'] }}% OFF</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 flash_next_pro1">
								<div class="row">
									<div class="col-md-offset-1 col-md-10 col-sm-offset-2 col-sm-8 col-xs-12">
										<img src="{{ url('components/front/images/product') }}/{{ $nx_flash['product_id'] }}/{{ $nx_flash['image'] }}" class="img-responsive img_center" />
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="flash_next_pro_tl">{{ $nx_flash['product_name'] }}</p>
							</div>

							@php
								$disc = $nx_flash['discount'] / 100;

								$disc_amount = $nx_flash['price'] * $disc;

								$disc_res = $nx_flash['price'] - $disc_amount;
							@endphp

							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="flash_pro_disc">Rp {{ number_format($disc_res),0,',','.' }}</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="flash_pro_rl">Rp {{ number_format($nx_flash['price']),0,',','.' }}</p>
							</div>

							
                          @if($nx_flash['product_stock'] >= $nx_flash['flash_stock'])
							@php
								$dates_next = date_create($nx_flash['tanggal']); 
								$dt_next = date_format($dates_next,"d - m - Y");
							@endphp

							<div class="col-md-12 col-sm-12 col-xs-12 center">
								<p class="flash_next_date">{{ $dt_next }}</p>
							</div>
						  @else
						    <div class="col-md-12 col-sm-12 col-xs-12 center">
								<p class="flash_next_date_stock">STOCK HABIS</p>
							</div>
						  @endif
						</div>
					</div>
					@endforeach
				@else
					<div class="col-md-12 col-sm-12 col-xs-12 center flash_next_none">
						<h3>Next Flash Deal belum ada, nantikan promo-promo kami nantinya.</h3>
					</div>
				@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
	$(window).on("load", function() {
		gettime();
	});

	function gettime(){
		var get_date = $('.time');

		get_date.each(function(i, v) {
			var ids = $(this).data('id');
			var dates = $(this).data('time');
			var text = $(this).data('text');

			time(dates, ids, text);
		});
	}

	function time(dates, ids, text) {
		// Set the date we're counting down to
		var countDownDate = new Date(dates).getTime();

		// Update the count down every 1 second
		var x = setInterval(function() {

		  // Get todays date and time
		  var now = new Date().getTime();

		  // Find the distance between now an the count down date
		  var distance = countDownDate - now;

		  // Time calculations for days, hours, minutes and seconds
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

		  // Display the result in the element with id="demo"
		  if(text != ""){
		  	document.getElementById("count_"+ids).innerHTML = text + " " + days + "d " + hours + "h "
			  + minutes + "m " + seconds + "s ";
   		  } 

		  // If the count down is finished, write some text 
		  if (distance < 0) {
		    clearInterval(x);
		    location.reload();
		  }
		}, 1000);
	}

</script>
@endpush