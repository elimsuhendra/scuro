@extends($view_path.'.layouts.shop_master')
@section('content_shop')
<div class="row shop_support_con">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="cus_container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12 sp_con1">
							@foreach($support as $q => $spt)
								<a href="#menu{{ $q }}" class="cus_links"><p><b>{{ ucfirst($spt->post_name) }}</b></p></a>
							@endforeach
						</div>
					</div>

					<div class="col-md-9 col-sm-7 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12 sp_con2">
							<h2><span class="sp_con2_1"><b>Sup</span>port</b></h2>
							
							@foreach($support as $k => $spt1)
							<div class="col-md-12 col-sm-12 col-xs-12 sp_con3">
								<div class="row">
									<h3 id="menu{{ $k }}">{{ ucfirst($spt1->post_name) }}</h3>
									<p>{!! $spt1->content !!}</p>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
        });
      }
    }
  });
</script>
@endpush