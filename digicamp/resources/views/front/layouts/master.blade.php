<!DOCTYPE html>
<html lang="id">
	<head>
		@include($view_path.'.includes.head')
	</head>
	<body>
		<div class="container-fluid">
			@include($view_path.'.includes.header')
			
			@yield('content')

			@include($view_path.'.includes.footer')
		</div>
	</body>
</html>