<!DOCTYPE html>
<html lang="id">
	<head>
		@include($view_path.'.includes.head_shop')
	</head>
	<body>
		<div class="container-fluid">
			@include($view_path.'.includes.header_shop')
			
			@yield('content_shop')

			@include($view_path.'.includes.footer_shop')
		</div>
	</body>
</html>