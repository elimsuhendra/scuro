<div class="row footer center">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <p>Copyright &copy; 2017. Scuro Lavino . All right reserved</p>
    </div>
</div>

<!-- Script -->
<script type="text/javascript" src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/jquery.matchHeight.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script> 
<script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script> 
<script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script> 
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stack('custom_scripts')

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5809d4de1e35c727dc0b1ca5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->