<div class="row header_con">
    <div class="col-md-12 col-sm-12 col-xs-12 head_con" />
        <div class="row cus_container">
            <!-- Logo -->
            <div class="col-md-3 col-sm-3 col-xs-8 head_1">
              <a href="{{ url('/') }}">
                <img src="{{ url('/components/both/images/web/') }}/{{ $web_logo }}" class="img-responsive img_center" />
              </a>
            </div>
            <!-- End Logo -->

            <!-- Menu -->
            <div class="col-sm-4 col-xs-4 head_2">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" id="menu_button">
                <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
              </button>
            </div> 

            <div class="col-md-9 col-sm-9 col-xs-12 head_3">
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right head_4">
                    <li><a class="{{ Request::segment(1) == "" ? 'cus_active' : ''}}" href="{{ url('/') }}"><span >Home</span></a></li>
                    @foreach($menus as $menu)
                        <li><a class="{{ Request::segment(1) == $menu->pages_name_alias ? 'cus_active' : '' }}" href="{{ url('/'.$menu->pages_name_alias) }}"><span >{{ $menu->pages_name }}</span></a></li>
                    @endforeach
                    <li><a class="{{ Request::segment(1) == "shop-now" ? 'cus_active' : ''}}" href="{{ url('/shop') }}"><span>Shop Now</span></a></li>
                </ul>
              </div>
            </div>
            <!-- End Menu -->
        </div>
    </div>
</div>
<!-- End Header -->

