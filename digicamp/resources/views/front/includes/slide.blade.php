<div class="row hm_con1 full-screen-preview__frame">
 	<div class="swiper-container">
        <div class="swiper-wrapper">
        	@foreach($slideshow as $slide)
            <div class="swiper-slide cus_slider">
        		<img src="{{ url('components/front/images/slideshow') }}/{{ $slide->image }}" class="img-responsive img_width full-screen-preview__frame" />

                <div class="slide_con1">
                    <div class="row slide_con1_1">
                        <div class="col-md-5 col-sm-7 col-xs-12 slide_con1_2"> 
                            <h1>{{ strtoupper($slide->slideshow_name) }}</h1>
                            <p>{{ $slide->description }}</p>

                            <a class="slide_con1_3" href="{{ $slide->url }}"><button>{{ strtoupper($slide->button_text) }}</button></a>
                        </div>
                    </div>
                </div>
        	</div>
            @endforeach
        </div> 

        <div class="swiper-pagination"></div>
    </div>
</div>