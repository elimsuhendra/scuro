<div class="row hm_con4 center">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <p>Copyright &copy; 2017. Scuro Lavino . All right reserved</p>
    </div>
</div>

<!-- Script -->
<script type="text/javascript" src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('components/plugins/owl-carousel/owl.carousel.min.js')}}"></script> -->
<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/jquery.matchHeight.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/custom.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script> -->
<!-- <script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script> -->
<!-- <script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script> -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stack('custom_scripts')