<div class="row head_1">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row cus_container flex_table">
      <div class="col-md-2 col-sm-4 col-xs-12 hd_logo">
        <div class="row">
          <a href="{{ url('/shop') }}">
            <img src="{{ url('/components/both/images/web/') }}/{{ $web_logo }}" class="img-responsive img_center" />
          </a>
        </div>
      </div>

      <div class="col-md-4 col-sm-8 col-xs-12 head_search">
        <form method="POST" action="search">
          {{csrf_field()}}
          
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search Product" id="hd_search" name="hd_search" autocomplete="off">

            <div class="input-group-btn">
              <button class="btn btn-default" type="submit">
                Search
              </button>
            </div>
          </div>
        </form>

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="rs_search"></div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="row head_login">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row hd_con_bt_cr {{ auth()->guard($guard)->check() ? 'flex_table' : '' }}">
              <div class="col-md-6 col-sm-6 col-xs-12 hd_button">
                <div class="row flex_table">
                  @if(auth()->guard($guard)->check())
                  <div class="col-lg-offset-2 col-lg-5 col-md-6 col-sm-6 col-xs-6 hd_shop_login1 dropdown">
                    <button class="btn btn-primary dropdown-toggle hd_shop_login2" type="button" data-toggle="dropdown"><b>Hi, {{ ucfirst(auth()->guard($guard)->user()->name) }}</b>
                    <span class="caret hd_shop_login3"></span></button>
                    <ul class="dropdown-menu hd_shop_login4">
                      <li><a href="{{ url('profile') }}"><b>Your Profile</b></a></li>
                      <li><a href="{{ url('order-histori') }}"><b>Order History</b></a></li>
                      <li><a href="{{ url('confirm-payment') }}"><b>Confirm Payment</b></a></li>
                    </ul>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                    <div class="row hd_shop_login5">
                      <a href="{{url('logout')}}"><b>Log Out</b></a>
                    </div>
                  </div>
                  @else
                  <div class="col-lg-offset-2 col-lg-5 col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <button type="button" class="btn btn-default hd_btn_login btn-block" data-toggle="modal" data-target="#modalLogin">Log In</button>
                    </div>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <button type="button" class="btn btn-default hd_btn_register btn-block" data-toggle="modal" data-target="#modalRegister">Register</button>
                    </div>
                  </div>
                  @endif
                </div>
              </div> 

              <div class="col-md-6 col-sm-6 col-xs-12 hd_mn_cart">
                <div class="row flex_table {{  auth()->guard($guard)->check() ? 'margin_cus' : '' }}">
                  <div class="col-md-3 col-sm-4 col-xs-4">
                    <div class="row">
                      <img src="{{ url('/components/front/images/icon/cart.jpg') }}" class="img-responsive img_center" />
                    </div>
                  </div>

                  <div class="col-md-9 col-sm-8 col-xs-8">
                    <div class="row hd_shop_cart">
                      <a href="{{url('cart')}}" class="header-functionality-entry open-cart-popup floating-badge hd_3_7"><b>Shopping Cart (<span class="total-cart">0</span>)</b></a>
                    </div>
                  </div>

                  <div class="col-md-12 col-sm-12">
                    <!-- Pop up cart -->
                    @if(request::path() != 'cart' && request::path() != 'checkout')
                    <div class="cart-box popup">
                      <div class="popup-container">
                          <div class="loader-container mini-cart-loader">
                            <div id="floatingCirclesG">
                              <div class="f_circleG" id="frotateG_01"></div>
                              <div class="f_circleG" id="frotateG_02"></div>
                              <div class="f_circleG" id="frotateG_03"></div>
                              <div class="f_circleG" id="frotateG_04"></div>
                              <div class="f_circleG" id="frotateG_05"></div>
                              <div class="f_circleG" id="frotateG_06"></div>
                              <div class="f_circleG" id="frotateG_07"></div>
                              <div class="f_circleG" id="frotateG_08"></div>
                            </div>
                          </div>
                          <div class="mini-cart"></div>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row head_2">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row cus_container flex_table head_2_1">
    @foreach($category_shop as $q => $cate_shop)
      <div class="col-md-2 col-sm-3 center head_2_hover"">
        <a href="{{ url('/shop/category') }}/{{ $cate_shop['name_alias'] }}"><p>{{ $cate_shop["name"] }}</p></a>
        <div class="head_under"></div>
        
        @if($cate_shop["sub_category"] != "")
        <div class="col-md-12 col-sm-12 head_hidden">
          <div class="row cus_container flex_table head_hidden_con">
            @foreach($cate_shop["sub_category"] as $sub_shop)
              @if($sub_shop["sub_sub_category"] != "")
              <div class="col-md-3 col-sm-3 head_hiddens">
                <div class="row">
                  <div class="col-md-12 col-sm-12 head_hidden_1">
                    <a href="{{ url('/shop/category') }}/{{ $sub_shop['name_alias'] }}"><p>{{ $sub_shop["name"] }}</p></a>
                  </div>

                  @foreach($sub_shop["sub_sub_category"] as $sub2_shop)
                  <div class="col-md-12 col-sm-12 head_hidden_2">
                    <a href="{{ url('/shop/category') }}/{{ $sub2_shop['name_alias'] }}"><p>{{ $sub2_shop["name"] }}</p></a>
                  </div>
                  @endforeach
                </div>
              </div>
              @else
              <div class="col-md-3 col-sm-3 head_hiddens">
                <div class="row">
                  <div class="col-md-12 col-sm-12 head_hidden_1">
                    <a href="{{ url('/shop/category') }}/{{ $sub_shop['name_alias'] }}"><p>{{ $sub_shop["name"] }}</p></a>
                  </div>
                </div>
              </div>
              @endif
            @endforeach
          </div>
        </div>
        @endif
      </div>
    @endforeach
    </div>
  </div>
</div>

<div class="row head_3">
  <div class="col-xs-12">
    <div class="row cus_container flex_table head_3_1 panel-group" id="mn_accordion">
      @foreach($category_shop as $q => $cate_shop)
      <div class="col-xs-12 panel">
        <button type="button" class="btn btn-info head_cus_caret_{{ $q }}" data-toggle="collapse" data-parent="#mn_accordion" href="#mn_{{ $q }}">{{ $cate_shop["name"] }} 
        
        @if($cate_shop["sub_category"] != "")
          <i class="fa fa-caret-down hd_caret" aria-hidden="true"></i>
        @endif
        </button>
        
        @if($cate_shop["sub_category"] != "")
        <div class="row head_3_hide panel-collapse collapse head_cus_content_c" id="mn_{{ $q }}">
          @foreach($cate_shop["sub_category"] as $k => $sub_shop)
            @if($sub_shop["sub_sub_category"] != "")
              <div class="head_3_cus_sub panel-group" id="mn_accordion2">
                <div class="col-xs-12 panel">
                  <button type="button" class="btn btn-info head_cus_caret_s_{{ $k }}" data-toggle="collapse" data-parent="#mn_accordion2" href="#mns_{{ $k }}">{{ $sub_shop["name"] }} 
        
                  @if($sub_shop["sub_sub_category"] != "")
                    <i class="fa fa-caret-down hd_caret" aria-hidden="true"></i>
                  @endif
                  </button>

                  @foreach($sub_shop["sub_sub_category"] as $sub2_shop)
                  <div class="col-xs-12 panel-collapse collapse head_cus_content_c" id="mns_{{ $k }}">
                    <div class="col-xs-12 head_3_content_sub">
                      <div class="row">
                        <a href="{{ url('/shop/category') }}/{{ $sub2_shop['name_alias'] }}"><p>{{ $sub2_shop["name"] }}</p></a>
                      </div>
                    </div>
                  </div>
                  @endforeach 
                </div>
              </div> 
            @else
              <div class="col-xs-12 head_3_content">
                <div class="row">
                  <a href="{{ url('/shop/category') }}/{{ $cate_shop['name_alias'] }}"><p>{{ $sub_shop["name"] }}</p></a>
                </div>
              </div>
            @endif
          @endforeach
        </div>
        @endif
      </div>
      @endforeach
    </div>
  </div>
</div>

<!-- modal Login -->
<div id="modalLogin" class="modal fade" role="dialog">
  <div class="modal-dialog cus_modal">

    <!-- Modal content-->
    <div class="modal-content cus_modalCon">
      <div class="modal-body">
        <div class="row flex_table">
          <div class="col-md-6 col-sm-6 col-xs-6 cus_modalHide">
            <div class="row">
              <img src="{{ url('/components/front/images/other/modal.jpg') }}" class="img-responsive img_center img_width" />
            </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 cus_modal_input">
            <form method="post" action="{{url('do-login')}}">
              @include('front.includes.errors')
              
              <div class="row cus_modal_input1">
                <p class="cus_modal_tl">Masuk</p>

                <div class="form-group">
                  <label class="cus_modal_label">EMAIL</label>
                  <input type="email" class="form-control" placeholder="Your Email" name="email" required>
                    <div class="error-text text-danger">{{$errors->login->first('email')}}</div>
                </div>

                <div class="form-group">
                  <label class="cus_modal_label">PASSWORD</label>
                  <input type="password" class="form-control" placeholder="Your Password" name="password" required>
                    <div class="error-text text-danger">{{$errors->login->first('password')}}</div>
                </div>

                <div class="form-group flex_table"> 
                  <div class="col-sm-4">
                    <div class="row cus_modal_submit">
                      <button type="submit" class="btn">Login</button>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="row cus_modal_link">
                      <a href="#" data-toggle="modal" data-target="#modalForgot" data-dismiss="modal">Lupa Password</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Login -->

<!-- modal Register -->
<div id="modalRegister" class="modal fade" role="dialog">
  <div class="modal-dialog cus_modal">

    <!-- Modal content-->
    <div class="modal-content cus_modalCon">
      <div class="modal-body">
        <div class="row flex_table">
          <div class="col-md-6 col-sm-6 col-xs-6 cus_modalHide">
            <div class="row">
              <img src="{{ url('/components/front/images/other/modal.jpg') }}" class="img-responsive img_center img_width" />
            </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 cus_modal_input">
            <form method="post" action="{{url('do-register')}}">
              @include('front.includes.errors')
              <div class="row cus_modal_input1">
                <p class="cus_modal_tl">Sign Up Today !</p>

                <div class="form-group">
                  <label class="cus_modal_label">FULL NAME</label>
                  <input type="text" class="form-control" placeholder="Your Name" name="name" required>
                    <div class="error-text text-danger">{{$errors->login->first('name')}}</div>
                </div>

                <div class="form-group">
                  <label class="cus_modal_label">EMAIL ADDRESS</label>
                  <input type="email" class="form-control" placeholder="Your Email" name="email" required>
                    <div class="error-text text-danger">{{$errors->login->first('email')}}</div>
                </div>

                <div class="form-group">
                  <label class="cus_modal_label">PASSWORD</label>
                  <input type="password" class="form-control" placeholder="Your Password" name="password" required>
                    <div class="error-text text-danger">{{$errors->login->first('password')}}</div>
                </div>

                <div class="form-group"> 
                  <div class="checkbox">
                    <label class="cus_modal_label2"><input type="checkbox" name="subscribe" checked="true"> <b>Subscribe</b></label>
                  </div>
                </div>

                <div class="form-group flex_table"> 
                  <div class="col-sm-4">
                    <div class="row cus_modal_submit">
                      <button type="submit" class="btn">Register</button>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="row cus_modal_link">
                      <a href="#" data-toggle="modal" data-target="#modalLogin" data-dismiss="modal">Login</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Register -->

<!-- modal Forgot Password -->
<div id="modalForgot" class="modal fade" role="dialog">
  <div class="modal-dialog cus_modal">

    <!-- Modal content-->
    <div class="modal-content cus_modalCon">
      <div class="modal-body">
        <div class="row flex_table">
          <div class="col-md-6 col-sm-6 col-xs-6 cus_modalHide">
            <div class="row">
              <img src="{{ url('/components/front/images/other/modal.jpg') }}" class="img-responsive img_center img_width" />
            </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 cus_modal_input">
            <form method="post" action="{{url('forgot-password')}}">
              @include('front.includes.errors')
              
              <div class="row cus_modal_input1">
                <p class="cus_modal_tl">Forgot Your Password</p>

                <div class="form-group">
                  <label class="cus_modal_label">EMAIL</label>
                  <input type="email" class="form-control" placeholder="Your Email" name="email" required>
                    <div class="error-text text-danger">{{$errors->login->first('email')}}</div>
                </div>

                <div class="form-group flex_table"> 
                  <div class="col-sm-4">
                    <div class="row cus_modal_submit">
                      <button type="submit" class="btn">Submit</button>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="row cus_modal_link">
                      <a href="#" data-toggle="modal" data-target="#modalLogin" data-dismiss="modal">Login</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Forgot Password -->

@push('custom_scripts')
<script>
$(document).ready(function(){
  $(".head_cus_content_c").on("show.bs.collapse", function(){
    var ids = $(this).attr('id');
    var arr = ids.split('_');
    
    $('.head_cus_caret_'+ arr +' i').removeClass('fa-caret-down');
    $('.head_cus_caret_'+ arr +' i').addClass('fa-caret-up');
  });

  $(".head_cus_content_c").on("hide.bs.collapse", function(){
    var ids = $(this).attr('id');
    var arr = ids.split('_');
    
    $('.head_cus_caret_'+ arr +' i').removeClass('fa-caret-up');
    $('.head_cus_caret_'+ arr +' i').addClass('fa-caret-down');
  });
});
</script>
@endpush

