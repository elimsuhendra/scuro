<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<head>
		<title>Invoices</title>
		<style>
			.container{
				max-width:600px;
				display:block;
				margin:auto;
			}

			.image img{
				max-height:400px;
				max-width:400px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<table width="100%">
				<tr>
					<td>
						<br>
						<h1 align="center">Recipes {{ $con1->post_name }}</h1>
						<br>
					</td>
				</tr>
				<tr>
					<td class="image" align="center">
						<img src="{{ asset('components/front/images/post') }}/{{ $con1->id }}/{{ $con1->thumbnail }}" onerror="this.src='{{ asset('components/both/images/not_found/our-recipes-not-found.png') }}';" class="img-responsive img_center" />
					</td>
				</tr>
				<tr>
					<td>
						{!! $con1->content !!}
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
