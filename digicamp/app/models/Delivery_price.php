<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Delivery_price extends Model{
	protected $table 				= 'delivery_price';
	protected $delivery_service_sub = 'digipos\models\Delivery_service_sub';

	public function delivery_service_sub(){
		return $this->belongsTo($this->delivery_service_sub,'delivery_service_sub_id');
	}
}
