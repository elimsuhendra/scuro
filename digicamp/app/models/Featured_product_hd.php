<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Featured_product_hd extends Model{
	protected $table 				= 'featured_product_hd';
	protected $featured_product_dt 	= 'digipos\models\Featured_product_dt';

	public function featured_product_dt(){
		return $this->hasMany($this->featured_product_dt,'featured_product_hd_id');
	}
}
