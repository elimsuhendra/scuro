<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_data_category extends Model{
	protected $table 	= 'product_data_category';
	protected $product 	= 'digipos\models\Product';

	public function product(){
		return $this->belongsTo($this->product,'product_id');
	}
}
