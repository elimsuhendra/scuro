<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model{
	protected $table 	= 'province';
	protected $city 	= 'digipos\models\City';

	public function city(){
		return $this->hasMany($this->city,'province_id','province_id');
	}
}
