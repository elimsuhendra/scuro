<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model{
	protected $table 						= 'voucher';
	protected $voucher_customer_group_data 	= 'digipos\models\Voucher_customer_group_data';

	public function voucher_customer_group_data(){
		return $this->hasMany($this->voucher_customer_group_data,'voucher_id');
	}
}
