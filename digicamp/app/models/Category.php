<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model{
	protected $table 	= 'category';
	protected $category = 'digipos\models\Category';
	protected $post     = 'digipos\models\Post';

	public function category(){
		return $this->belongsTo($this->category,'category_id');
	}

	public function subcategory(){
		return $this->hasMany($this->category,'category_id');
	}

	public function post(){
		return $this->hasMany($this->post, 'category_id');
	}
}
