<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Delivery_service extends Model{
	protected $table 				= 'delivery_service';
	protected $delivery_service_sub = 'digipos\models\Delivery_service_sub';

	public function delivery_service_sub(){
		return $this->hasMany($this->delivery_service_sub,'delivery_service_id');
	}
}
