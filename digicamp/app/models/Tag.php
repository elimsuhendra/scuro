<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model{
	protected $table 		= 'tag';
	protected $post_tag 	= 'digipos\models\Post_tag';

	public function post_tag(){
		return $this->hasMany($this->post_tag,'tag_id');
	}
}
