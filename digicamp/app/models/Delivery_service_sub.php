<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Delivery_service_sub extends Model{
	protected $table 				= 'delivery_service_sub';
	protected $delivery_service 	= 'digipos\models\Delivery_service';
	protected $delivery_price 		= 'digipos\models\Delivery_price';

	public function delivery_service(){
		return $this->belongsTo($this->delivery_service,'delivery_service_id');
	}

	public function delivery_price(){
		return $this->hasMany($this->delivery_price,'delivery_service_sub_id');
	}
}
