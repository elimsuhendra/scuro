<?php namespace digipos\models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Customer extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    protected $hidden 				= ['password', 'remember_token'];

	protected $table 				= 'customer';
	protected $customer_address 	= 'digipos\models\Customer_address';
	protected $customer_group_data 	= 'digipos\models\Customer_group_data';

	public function customer_group_data(){
		return $this->hasMany($this->customer_group_data,'customer_id');
	}

	public function primary_address(){
		return $this->hasOne($this->customer_address,'customer_id')->where('status','y');
	}

	public function customer_address(){
		return $this->hasMany($this->customer_address,'customer_id');
	}
}
