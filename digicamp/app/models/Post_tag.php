<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Post_tag extends Model{
	protected $table 	= 'post_tag';
	protected $tag 		= 'digipos\models\Tag';

	public function tag(){
		return $this->belongsTo($this->tag,'tag_id');
	}
}
