<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Orderhd extends Model{
	protected $table 			= 'orderhd';
	protected $orderdt 			= 'digipos\models\Orderdt';
	protected $orderaddress 	= 'digipos\models\Orderaddress';
	protected $orderbilling 	= 'digipos\models\Orderbilling';
	protected $orderstatus 		= 'digipos\models\Orderstatus';
	protected $orderconfirm 	= 'digipos\models\Orderconfirm';
	protected $orderlog 		= 'digipos\models\Orderlog';
	protected $customer 		= 'digipos\models\Customer';
	protected $payment_method 	= 'digipos\models\Payment_method';

	public function orderdt(){
		return $this->hasMany($this->orderdt,'orderhd_id');
	}
	
	public function orderbilling(){
		return $this->hasOne($this->orderbilling,'orderhd_id');
	}

	public function orderaddress(){
		return $this->hasOne($this->orderaddress,'orderhd_id');
	}

	public function orderlog(){
		return $this->hasMany($this->orderlog,'orderhd_id');
	}
	
	public function orderstatus(){
		return $this->belongsTo($this->orderstatus,'order_status_id');
	}

	public function orderconfirm(){
		return $this->hasMany($this->orderconfirm,'orderhd_id')->orderBy('id','desc');
	}

	public function customer(){
		return $this->belongsTo($this->customer,'customer_id');
	}

	public function payment_method(){
		return $this->belongsTo($this->payment_method,'payment_method_id');
	}
}
