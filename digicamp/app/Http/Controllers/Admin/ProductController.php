<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Product;
use digipos\models\Product_data_category;
use digipos\models\Product_data_images;
use digipos\models\Product_category;
use digipos\models\Product_brand;
use digipos\models\Product_attribute;
use digipos\models\Product_attribute_data;
use digipos\models\Product_data_attribute_master;
use digipos\models\Product_data_attribute;
use digipos\models\Discount;


use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use File;
use DB;

class ProductController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.product');
		$this->root_link 		= "product";
		$this->primary_field 	= "name";
		$this->model 			= new Product;
		$this->bulk_action 		= true;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'thumbnail',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'belongto' => ['method' => 'thumbnail','field' => 'images_name'],
				'file_opt' => ['path' => 'components/front/images/product/','custom_path' => 'id']
			],[
				'name' => 'name',
				'label' => 'Product Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model->thumbnail = 'a';
		return $this->build('index');
	}


	public function create(){
		$this->data['title']	= trans('general.create').' '.$this->title;
		return $this->render_view('pages.product.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(request $request){
		$this->validate($request,[
			'name'					=> 'required|unique:product',
			'product_code'			=> 'required',
		]);
		$this->model->name 				= $request->name;
		$this->model->name_alias 		= $this->build_alias($request->name);
		$this->model->product_code 		= $request->product_code;
		$this->model->description 		= $request->description;
		$this->model->content 			= $request->content;
		$this->model->width	 			= 0;
		$this->model->height	 		= 0;
		$this->model->depth	 			= 0;
		$this->model->weight	 		= 0;
		$this->model->price	 			= 0;
		$this->model->additional_fees 	= 0;
		$this->model->status 			= 'n';
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		$this->model->save();

		$file 	= [
					'name' 		=> 'image',
					'file_opt' 	=> ['path' => 'components/front/images/product/'.$this->model->id.'/'],
				];

		if($request->hasFile('image')){
			$image = $this->build_image($file);
			$this->model->image = $image;
			$this->model->save();
		}else{
			if($request->has('remove-single-image-image') == 'y'){
				File::delete($file['file_opt']['path'].$this->model->image);
				$this->model->image = '';
				$this->model->save();
			}
		}

		Alert::success('Successfully add new product');
		return redirect()->to($this->data['path'].'/'.$this->model->id.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['product']  				= $this->model->find($id);
		$this->data['product_data_category']	= $this->data['product']->product_data_category->pluck('product_category_id')->toArray();
		$this->data['product_data_images']		= $this->data['product']->product_data_images;
		$this->data['category'] 				= $this->generateCategory();
		// $this->data['category'] 				= Product_category::where('category_id',0)->where('status','y')->orderBy('category_name','asc')->get();
		$this->data['brand']					= Product_brand::where('status','y')->orderBy('brand_name','asc')->get();
		$this->data['product_attribute']		= Product_attribute::orderBy('order','asc')->pluck('attribute_name','id')->toArray();
		$this->data['product_data_attribute_master']	= Product_data_attribute_master::where('product_id',$id)->with('product_data_attribute.product_attribute_data.attribute')->get();
		$this->data['title']					= trans('general.view').' '.$this->title.' '.$this->data['product']->name;
		$this->data['discount']					= $this->data['product']->product_data_discount;
		return $this->render_view('pages.product.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['product']  				= $this->model->find($id);
		$this->data['product_data_category']	= $this->data['product']->product_data_category->pluck('product_category_id')->toArray();
		$this->data['product_data_images']		= $this->data['product']->product_data_images;
		$this->data['category'] 				= $this->generateCategory();
		// $this->data['category']		            = Product_category::where('category_id',0)->where('status','y')->orderBy('category_name','asc')->get();
		$this->data['brand']					= Product_brand::where('status','y')->orderBy('brand_name','asc')->get();
		$this->data['product_attribute']		= Product_attribute::orderBy('order','asc')->pluck('attribute_name','id')->toArray();
		$this->data['product_data_attribute_master']	= Product_data_attribute_master::where('product_id',$id)->with('product_data_attribute.product_attribute_data.attribute')->get();
		$this->data['title']					= trans('general.edit').' '.$this->title.' '.$this->data['product']->name;
		$this->data['discount']					= $this->data['product']->product_data_discount;
		return $this->render_view('pages.product.edit');
	}

	public function generateCategory(){
        $get_cat = Product_category::where('status', 'y')->where('category_id', 0)->orderBy('id', 'asc')->get();
        $result = [];

        foreach($get_cat as $q => $gets){
            $category = $this->setCategory($gets->id);

            $result[$q] = [
                            'id'            => $gets->id,
                            'category_id'	=> $gets->category_id,
                            'name'          => $gets->category_name,
                            'sub_category'  => $category,
                          ]; 
        }

        return $result;
    }

    public function setCategory($gets){
        $get_cat = Product_category::where('category_id', $gets)->get();

        if(count($get_cat) > 0){
            foreach($get_cat as $k => $gets2){
                $category = $this->setSubCategory($gets2->id);

                $result[$k] = [
                                'id'            => $gets2->id,
                            	'category_id'	=> $gets2->category_id,
                                'name'          => $gets2->category_name,
                                'sub_sub_category'  => $category,
                              ];
            }
        } else{
            $result = '';
        }

        return $result;
    }

    public function setSubCategory($gets){
        $get_cat = Product_category::where('category_id', $gets)->get();

        if(count($get_cat) > 0){
            foreach($get_cat as $k => $gets2){
                $result[$k] = [
                                'id'            => $gets2->id,
                            	'category_id'	=> $gets2->category_id,
                                'name'          => $gets2->category_name,
                              ];
            }
        } else{
            $result = '';
        }
        

        return $result;
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'name'					=> 'required|unique:product,id,'.$id,
			'product_code'			=> 'required',
			'product_category_id'	=> 'required',
			'status'				=> 'required'
		]);
		$this->model 					= $this->model->find($id);
		$this->model->name 				= $request->name;
		$this->model->name_alias 		= $this->build_alias($request->name);
		$this->model->product_code 		= $request->product_code;
		$this->model->product_brand_id 	= $request->product_brand_id;
		$this->model->product_category_id 	= $request->product_category_id;
		$this->model->description 		= $request->description;
		$this->model->content 			= $request->content;
		$this->model->width	 			= $request->width;
		$this->model->height	 		= $request->height;
		$this->model->depth	 			= $request->depth;
		$this->model->weight	 		= $request->weight;
		$this->model->price	 			= $request->price;
		$this->model->additional_fees 	= $request->additional_fees;
		$this->model->meta_title 		= $request->meta_title;
		$this->model->meta_keywords 	= $request->meta_keywords;
		$this->model->meta_description 	= $request->meta_description;
		$this->model->status 			= $request->status;
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;

		$file 	= [
					'name' 		=> 'image',
					'file_opt' 	=> ['path' => 'components/front/images/product/'.$this->model->id.'/'],
				];

		if($request->hasFile('image')){
			$image = $this->build_image($file);
			$this->model->image = $image;
		}else{
			if($request->input('remove-single-image-image') == 'y'){
				File::delete($file['file_opt']['path'].$this->model->image);
				$this->model->image = '';
			}
		}

		$this->model->save();


		if($request->discount_status == "y"){
			$check = Discount::where('product_id', $id)->first();

			if(count($check) > 0){
				$get_from 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[0]));
				$get_to 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[1]));
				$conv_from					= date_format(date_create($get_from),'Y-m-d');
				$conv_to					= date_format(date_create($get_to),'Y-m-d');
				$amount						= $request->amount ? $request->amount : 0;
				$get_upd					= auth()->guard($this->guard)->user()->id;

				Discount::where('product_id', $id)->update([
															'valid_from' 		=> $conv_from,
															'valid_to' 			=> $conv_to,
															'discount_type' 	=> $request->discount_type,
															'amount'			=> $amount,
															'status'			=> $request->discount_status,
															'upd_by'			=> $get_upd
															]);
			} else {
				$discount = new Discount;
				$discount->product_id 		= $id;
				$valid_from 				= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[0]));
				$valid_to 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[1]));
				$discount->valid_from		= date_format(date_create($valid_from),'Y-m-d');
				$discount->valid_to			= date_format(date_create($valid_to),'Y-m-d');
				$discount->discount_type 	= $request->discount_type;
				$discount->amount			= $request->amount ? $request->amount : 0;
				$discount->status 			= $request->discount_status;
				$discount->upd_by			= auth()->guard($this->guard)->user()->id;
				$discount->save();
			}
		}

		//Images
		if($request->has('images_name_hidden_data')){
			$i 	= 1;
			foreach($request->images_name_hidden_data as $hd){
				Product_data_images::where('id',$hd)->update(['order' => $i++,'status' => isset($request->images_name_status[$hd]) ? 'y' : 'n','status_primary' => $request->images_name_status_primary == $hd ? 'y' : 'n']);
			}
		}

		//Category
		if(count($request->product_data_dategory_id) > 0){
			foreach($request->product_data_dategory_id as $pdd){
				$product_data_category[] 	= [
												'product_id'			=> $this->model->id,
												'product_category_id'	=> $pdd
											];
			}
			Product_data_category::where('product_id',$this->model->id)->delete();
			$product_data_category 			= Product_data_category::insert($product_data_category);
		}

		Alert::success('Successfully edit '.$this->model->post_name);
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(request $request){
		$product 	= $this->model->find($request->id);
		$attribute 	= $product->product_data_attribute_master;
		foreach($attribute as $a){
			$a->product_data_attribute()->delete();
			$a->delete();
		}
		$product->product_data_category()->delete();
		$product->product_data_discount()->delete();
		$product->product_data_images()->delete();
		$product->product_data_attribute_master()->delete();
		$product->delete();
		Alert::success('Product has been deleted');
		return redirect()->back();
	}

	public function ext(request $request,$action){
		$this->data['request']	= $request;
		return $this->$action();
	}

	public function upload_image(){
		$request 	= $this->data['request'];
		$id 		= $request['id'];
		$file_path 	= $request['file_path'];
		$crop 		= $request['crop'];
		//Upload Thumbnail
		$file 	= [
					'name' 		=> 'images_upload',
					'file_opt' 	=> ['path' => $file_path],
					'crop'		=> isset($crop) && $crop == 'y' ? 'y' : 'n'
				];
		$image = $this->build_image($file);
		$product_data_images 				= new Product_data_images;
		$product_data_images->product_id 	= $id;
		$product_data_images->images_name 	= $image;
		$cek   = Product_data_images::where('product_id',$id)->orderBy('order','desc')->first();
		$product_data_images->status 	= 'y';
		if($cek){
			$product_data_images->status_primary 	= 'n';
			$product_data_images->order 			= $cek->order+1;
		}else{
			$product_data_images->status_primary 	= 'y';
			$product_data_images->order 			= 1;
		}
		$product_data_images->save();
		$res['image']	= $image;
		return response()->json($res);
	}

	public function delete_image(){
		$request 	= $this->data['request'];
		$id 		= $request['id'];
		Product_data_images::where('id',$id)->delete();
		$res 		= ['status'	=> 'continue'];
		return response()->json($res);
	}

	public function attribute_value(){
		$request 	= $this->data['request'];
		$id 		= $request['id'];
		$value 		= Product_attribute_data::where('attribute_id',$id)->orderBy('order','asc')->pluck('name','id');
		$res 		= ['status'	=> 'continue','value' => $value];
		return response()->json($res);
	}

	public function save_combination(){
		$request 	= $this->data['request'];
		$id 		= $request->id;
		$data 		= $request->data;
		$stock 		= isset($request->stock) && $request->stock > 0 ? $request->stock : 0;
		//Check Existing Data
		$check 				= Product_data_attribute_master::where('product_id',$id)->with('product_data_attribute')->get();
		
		// if(count($check) == 0){
			foreach($check as $c){
				$temp = [];
				foreach($c->product_data_attribute as $pc){
					$temp[]	= (string)$pc->product_attribute_data_id;
				}
				$result 	= array_intersect($temp,$data);
				if($temp === array_intersect($temp,$data) && $data === array_intersect($data, $temp)){
					return response()->json(['status' => 'exist']); 
				}
			}

			//Master
			$ms 		 		= new Product_data_attribute_master;
			$ms->product_id 	= $id;
			$ms->stock 			= $stock;
			$ms->hold 			= 0;
			$ms->sold 			= 0;
			$ms->save();

			//data
			foreach($data as $d){
				$datax[] 		= [
									'product_data_attribute_master_id' 	=> $ms->id,
									'product_attribute_data_id'			=> $d,
									'created_at'						=> date('Y-m-d H:i:s')
								];
			}
			Product_data_attribute::insert($datax);
			return response()->json(['status' => 'continue']);
		// } else{
		// 	return response()->json(['status' => 'limit']);
		// }
	}

	public function edit_combination(){
		$request 				= $this->data['request'];
		$id 					= $request->id;
		$data['combination']	= Product_data_attribute_master::where('id',$id)->with('product_data_attribute.product_attribute_data.attribute')->first();
		return response()->json($data);
	}

	public function update_combination(){
		$request 	= $this->data['request'];
		$id 		= $request->id;
		$data 		= $request->data;
		$stock 		= isset($request->stock) && $request->stock > 0 ? $request->stock : 0;
		$combination_id = $request->combination_id;
		$action_change 	= $request->action_change;
		Product_data_attribute_master::where('id',$combination_id)->update(['stock' => $stock]);
		if($action_change == 'y'){
			//Check Existing Data
			$check 				= Product_data_attribute_master::where('product_id',$id)->with('product_data_attribute')->get();
			foreach($check as $c){
				$temp = [];
				foreach($c->product_data_attribute as $pc){
					$temp[]	= (string)$pc->product_attribute_data_id;
				}
				$result 	= array_intersect($temp,$data);
				if($temp === array_intersect($temp,$data) && $data === array_intersect($data, $temp)){
					return response()->json(['status' => 'exist']); 
				}
			}

			//data
			foreach($data as $d){
				$datax[] 		= [
									'product_data_attribute_master_id' 	=> $combination_id,
									'product_attribute_data_id'			=> $d,
									'created_at'						=> date('Y-m-d H:i:s')
								];
			}
			if(count($datax > 0)){
				Product_data_attribute::where('product_data_attribute_master_id',$combination_id)->delete();
				Product_data_attribute::insert($datax);
			}
		}
		return response()->json(['status' => 'continue']);
	}

	public function delete_combination(){
		$request 	= $this->data['request'];
		$id 		= $request->id;
		$master 	= Product_data_attribute_master::find($id);
		$master->product_data_attribute()->delete();
		$master->delete();
		return response()->json(['data' => 'continue']);
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => 'components/front/images/product/','custom_path' => 'product_id']
							],[
								'name' => 'product_code',
								'type' => 'text'
							],[
								'name' => 'name',
								'type' => 'text'
							]
						];
		$this->order_method = "multiple";
		$this->query_method = "raw";
		$this->order_filter = [
								'label' => 'Category',
								'name' => 'id',
								'data' => $this->get_category(),
								];
	}

	public function sorting(){
		$request 		= $this->data['request'];
		$this->model = DB::table('product_data_category as a')
						->join('product as b','a.product_id','=','b.id')
						->leftjoin('product_data_images as c','b.id','=','c.product_id')
						->where('a.product_category_id',$request->id)
						->groupBy('b.id')
						->orderBy('a.order','asc')
						->select('a.*','b.name','b.product_code','c.images_name as image','b.id as product_id');
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		$this->sorting_config();
		$this->model = new Product_data_category;
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_category(){
		$query = Product_category::where('status','y')->orderBy('order','asc')->pluck('category_name','id')->toArray();
		return $query;
	}
}
