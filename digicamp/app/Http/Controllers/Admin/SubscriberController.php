<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Subscriber;

class SubscriberController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = trans('general.subscriber');
		$this->root_link = "subscriber";
		$this->primary_field = "email";
		$this->model = new Subscriber;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'email',
				'label' => trans('general.email'),
				'sorting' => 'y',
				'search' => 'text'
			]
		];
		return $this->build('index');
	}

	public function ext($action){
		return $this->$action();
	}

	public function export(){
		return $this->build_export();
	}
}
