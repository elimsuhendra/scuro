<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\User;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;

class UserController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "User";
		$this->primary_field 	= "name";
		$this->root_link 		= "user";
		$this->model 			= new User;
		$this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [4];
		$this->image_path 		= 'components/back/images/admin/';
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'images',
				'label' => 'Photo',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'email',
				'label' => 'Email',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'name',
				'label' => 'Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'user_access_id',
				'label' => 'User Access',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => $this->get_user_access(),
				'belongto' => ['method' => 'useraccess','field' => 'access_name']
			],[
				'name' => 'status',
				'label' => 'Status',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y'
					],[
						'name' => 'user_access_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => $this->get_user_access(),
						'class'	=> 'select2',
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'name',
						'label' => 'User Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'description',
						'label' => 'About User',
						'type' => 'textarea',
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'images',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
					],[
						'name' => 'language_id',
						'label' => 'Language',
						'type' => 'radio',
						'data' => $this->get_language(),
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'status',
						'label' => 'Status User',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					]
					/*,[
						'name' => 'fkmsmenuid',
						'label' => 'Status User',
						'type' => 'checkbox',
						'hasmany' => ['method' => 'msmenu','primary'=>'','field' => 'fkmsmenuid','table' => new Tbstatus], 
						'data' => $this->build_array(Tbmsmenu::get(),'id','nama_menu')
					]*/
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'hash' => 'y'
					],[
						'name' => 'user_access_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => $this->get_user_access(),
						'class'	=> 'select2',
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'name',
						'label' => 'User Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'description',
						'label' => 'About User',
						'type' => 'textarea',
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'images',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
					],[
						'name' => 'language_id',
						'label' => 'Language',
						'type' => 'radio',
						'data' => $this->get_language(),
						'attribute' => 'required',
						'validation' => 'required'
					],[
						'name' => 'status',
						'label' => 'Status User',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					]
					/*,[
						'name' => 'fkmsmenuid',
						'label' => 'Status User',
						'type' => 'checkbox',
						'hasmany' => ['method' => 'msmenu','primary'=>'','field' => 'fkmsmenuid','table' => new Tbstatus], 
						'data' => $this->build_array(Tbmsmenu::get(),'id','nama_menu')
					]*/
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}
}
