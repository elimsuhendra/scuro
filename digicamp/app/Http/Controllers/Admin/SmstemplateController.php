<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Orderstatus;

class SmstemplateController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard); 
		$this->title = "Sms Template";
		$this->root_link = "sms-template";
		$this->primary_field = "order_status_name";
		$this->model = new Orderstatus;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'order_status_name',
				'label' => 'SMS Status',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status_sms_cust',
				'label' => 'Status Sms Customer',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			],[
				'name' => 'status_sms_admin',
				'label' => 'Status Sms Admin',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		if(!\Request::has('orderdata')){
			$this->model = $this->model->orderBy('id','asc');
		}
		return $this->build('index');
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'order_status_name',
						'label' => 'Order Status / Notification Status',
						'type' => 'text',
						'attribute' => 'required autofocus readonly',
					],[
						'name' => 'sms_admin',
						'label' => 'Sms Admin',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required',
					],[
						'name' => 'title',
						'label' => 'Title',
						'type' => 'text',
						'attribute' => 'required',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'required',
					],[
						'name' => 'subject',
						'label' => 'Subject',
						'type' => 'text',
						'attribute' => 'required',
						'form_class' => 'col-md-6 pad-right',
						'validation' => 'required',
					],[
						'name' => 'status_sms_cust',
						'label' => 'Activate sms to customer?',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'required'
					],[
						'name' => 'status_sms_admin',
						'label' => 'Activate sms to admin?',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'form_class' => 'col-md-6',
						'validation' => 'required'
					],[
						'name' => 'sms_cust_content',
						'label' => 'Customer sms content',
						'type' => 'textarea',
						'class' => 'editor',
						'form_class' => 'col-md-6 pad-left',
					],[
						'name' => 'sms_admin_content',
						'label' => 'Admin sms content',
						'type' => 'textarea',
						'class' => 'editor',
						'form_class' => 'col-md-6 pad-right',
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$this->model = $this->model->where('id',$id);
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}
}
