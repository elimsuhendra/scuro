<?php namespace digipos\Http\Controllers\Admin;
use digipos\Libraries\Email;

use digipos\models\Province;
use digipos\models\City;
use digipos\models\Sub_district;

class IndexController extends Controller {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard); 
		$this->data['title'] = "Dashboard";
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $curl = curl_init();

		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => "origin=151&originType=city&destination=2087&destinationType=subdistrict&weight=1000&courier=jne",
		//   CURLOPT_HTTPHEADER => array(
		//     "content-type: application/x-www-form-urlencoded",
		//     "key: c231455ad5320cbb2d670a39aef8fc8f"
		//   ),
		// ));

		// $response = curl_exec($curl);
		// $err = curl_error($curl);

		// curl_close($curl);

		// if ($err) {
		//   echo "cURL Error #:" . $err;
		// } else {
		//   dd(json_decode($response)->rajaongkir->results);
		// }
		// dd('x');
		/*Email::to('sinceritymaiden@hotmail.com');
		Email::subject('test');
		Email::view($this->view_path.'.emails.body-info');
		Email::email_data($this->data);
		Email::send();*/
		return $this->render_view('pages.index');
	}
}
