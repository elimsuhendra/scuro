<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Contactmessage;

class ContactmessageController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = trans('general.contact-message');
		$this->root_link = "contact-message";
		$this->primary_field = "name";
		$this->model = new Contactmessage;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'name',
				'label' => trans('general.name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'email',
				'label' => trans('general.email'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'subject',
				'label' => trans('general.subject'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'message',
				'label' => trans('general.message'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'updated_at',
				'label' => trans('general.created-at'),
				'sorting' => 'y',
				'search' => 'text'
			]
		];
		return $this->build('index');
	}

	public function ext($action){
		return $this->$action();
	}

	public function export(){
		return $this->build_export();
	}
}
