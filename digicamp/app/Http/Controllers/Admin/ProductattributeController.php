<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Product_attribute;

use Illuminate\Http\Request;
use digipos\Libraries\Alert;

class ProductattributeController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.product-attribute');
		$this->check_relation 	= ['attribute_data'];
		$this->root_link 		= "product-attribute";
		$this->primary_field 	= "attribute_name";
		$this->model 			= new Product_attribute;
		$this->bulk_action 		= true;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'attribute_name',
				'label' => 'Attribute Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'attribute_type',
				'label' => 'Attribute Type',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => $this->get_attribute_type(),
			]
		];
		return $this->build('index');
	}


	public function field_create(){
		$field = [
					[
						'name' => 'attribute_name',
						'label' => 'Attribute Name',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],[
						'name' => 'attribute_type',
						'label' => 'Attribute_type',
						'type' => 'select',
						'data' => $this->get_attribute_type(),
						'attribute' => 'required',
						'validation' => 'required'
					]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'attribute_name',
						'label' => 'Attribute Name',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],[
						'name' => 'attribute_type',
						'label' => 'Attribute_type',
						'type' => 'select',
						'data' => $this->get_attribute_type(),
						'attribute' => 'required',
						'validation' => 'required'
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'attribute_name',
								'type' => 'text'
							]
						];
	}

	public function sorting(){
		$this->model = $this->model->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function bulkupdate(){
		$a = $this->buildbulkedit();
		return $a;
	}

	public function get_attribute_type(){
		$a = ['color' => 'Color', 'select' => 'Select'];
		return $a;
	}
}
