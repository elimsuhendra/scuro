<?php namespace digipos\Http\Controllers\Admin;
use Request;
use Validator;
use Hash;
use Auth;
use Image;
use File;


class ServicesController extends KyubiController {

	public function index(){
		$act = Request::input('act');
		return $this->$act();
	}

	private function display_image(){
		$files = Request::file('images_upload');
		$mime = ['png','jpg','gif','jpeg'];
		$ext = $files->getClientOriginalExtension();
		if (!in_array(strtolower($ext), $mime)){
			return "ext";
		}else{
			$img = 'data:image/png;base64,'.base64_encode(file_get_contents($files->getRealPath()));
			return $img;
		}
	}
}
