<?php namespace digipos\Http\Controllers\Front;

// use Cache;
use digipos\models\Slideshow;
use digipos\models\Post;

use Illuminate\Http\request;

class IndexController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(request $request){
		$slideshow = Slideshow::where([['status', 'y'],['pages_id', 15]])->orderBy('id','asc')->get();
		$content1 = Post::where('category_id', 16)->first();

		$this->data['slideshow'] = $slideshow != NULL ? $slideshow : '';
		return $this->render_view('pages.index');
	}

	public function index_shop(request $request){
		$this->data['product'] = Product::with('product_data_attribute_master')->orderBy('product.id', 'desc')->select('product.*')->get();

		return $this->render_view('pages.shop.shop');
	}

	// public function search_live(request $request){
	// 	$search = $request->hd_search;

	// 	$this->data['slideshow'] = Slideshow::where('status', 'y')->orderBy('id','asc')->get();

	// 	$this->data['product'] = Product::where([['status', 'y'],['name', 'LIKE', $search.'%']])->with('product_data_attribute_master')->get();

	// 	$this->data['attr'] = Product::where([['status', 'y'],['name', 'LIKE', $search.'%']])->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();	
		
	// 	$this->data['hm_category'] = product_category::with('product_data_category')->where([['category_id', 0],['status', 'y']])->orderBy('id', 'desc')->get();

	// 	$this->data['banner'] = Category::with('subcategory', 'post')->where('id', 10)->get();

	// 	$this->data['video'] = Category::with('subcategory', 'post')->where('id', 12)->OrderBy('id', 'desc')->get();

	// 	return $this->render_view('pages.pages.search');
	// }
}
