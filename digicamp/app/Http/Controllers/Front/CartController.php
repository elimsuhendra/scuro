<?php namespace digipos\Http\Controllers\Front;

use Illuminate\Http\request;

use digipos\Libraries\Cart;

use digipos\models\Config;

use digipos\models\Product;
use digipos\models\Product_data_attribute_master;

use digipos\models\Delivery_service;

class CartController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function cart(request $request){
		$this->goes_well();
		return $this->render_view('pages.checkout.cart');
	}

	public function miniCart(request $request){
		$this->query_cart($request);
		return $this->render_view('pages.parts.mini_cart');
	}

	public function cartParts(request $request){	
		$status = [];
		$this->data['province'] =  json_decode($this->getprovince($request));
		$cart = Cart::get_cart(); 
		$get_coupon = Cart::get_coupon();

		if($get_coupon != NULL) {
            if(auth()->guard($this->guard)->check()){
				$get_st_coupon = $this->ch_check_voucher($get_coupon['id']);

				if($get_st_coupon['id'] == 1){
		    		$this->data['st_voucher'] = $get_st_coupon['message'];  
		    	} else if($get_st_coupon['id'] == 0){
		    		$this->data['st_voucher'] = "";
		    	}
		    } else{
		    	$this->data['st_voucher'] = "You Must Login First";
		    }	
    	}

    	if($cart != NULL) {
    		foreach($cart as $q => $ct){
				array_push($status, $q);
			}
    	}
		
		$this->data['status'] = Product_data_attribute_master::whereIn('id', $status)->get();
		$this->query_cart2($request);

		return $this->render_view('pages.parts.cart');
	}

	/* Custom Cart Controller */
	public function get_city(request $request){
		$id 	= $request->id;
		$request->request->add(['province' => $id]);
		$city 	= json_decode($this->getcity($request));
		return response()->json(['city' => $city]);
	}

	public function get_subdistrict(request $request){
		$id 	= $request->id;
		$request->request->add(['city' => $id]);
		$subdistrict 	= json_decode($this->getsubdistrict($request));
		return response()->json(['subdisctrict' => $subdistrict]);
	}

	public function get_courier(request $request){
		$id 	= $request->id;	
		$weight = $request->weight;

		$get_from = Config::where('name', 'city')->first();
		$from = $get_from->value;

		$request->request->add(['id' => $id, 'weight' => $weight, 'from' => $from]);
		$courier = json_decode($this->getcourier($request));

		$c_courier = Delivery_service::where('status', 'y')->with(['delivery_service_sub' => function ($query) {
    														$query->where('status', 'y');
    														}])->get();

		$check = [];
		$i = 0;

		foreach($c_courier as $ct) {
			foreach($ct->delivery_service_sub as $dl) {
				array_push($check, $dl->name_alias);
			}
			
		}

		foreach($courier->rajaongkir->results as $rj) {
			foreach($rj->costs as $cs) {
				if(in_array($cs->service, $check)) {
					$i++;
					if($cs->service == "CTC"){
						$shipping[$i] = [ 'service' => $cs->service,
										  'cost'    => $cs->cost[0],
										  'name'    => "Reg",
										  'code'    => $rj->code
										];
					} else if($cs->service == "CTCYES"){
						$shipping[$i] = [ 'service' => $cs->service,
										  'cost'    => $cs->cost[0],
										  'name'    => "Yes",
										  'code'    => $rj->code
										];
					} else{
					$shipping[$i] = [ 'service' => $cs->service,
									  'cost'    => $cs->cost[0],
									  'name'    => $cs->description,
									  'code'    => $rj->code
									];
					}
				}
			}
			
		}

		return response()->json(['courier' => $shipping]);
	}

	public function get_cost(request $request){
		$shipping = $request->value;
		$gettotal = Cart::get_checkout_data();

		if($gettotal["total"] == 0){
			$get_res = $gettotal["coupon_amount"] - $gettotal["sub_total"];

			if($get_res >= $shipping){
				$result = 0;
			} else if($shipping > $get_res){
				$result = $shipping - $get_res;
			}
		} else{
			$total = $gettotal["total"];
			$result = $total + $shipping;
		}
		
		return $result;
	}

	public function check_coupon(request $request){
		if(auth()->guard($this->guard)->check()){
			$result = $this->check_voucher($request);
		} else {
			$result = ['status' => 'failed', 'message' => 'You Must Login First'];
		}
		return response()->json(['result' => $result]);	
	}

	public function remove_coupon(request $request){
		$id 	= $request->id;
        $this->removed_coupon($id);
        return response()->json(['status' => 'success']);
	}
	/* End Custom */

	public function sideCartParts(request $request){
		$this->query_cart($request);
		return $this->render_view('pages.parts.side_cart');
	}

	public function query_cart($request){
		$this->data['cart'] 				= $this->generateCart($request);
		if($this->data['cart'] != 'no_data'){
			$this->data['checkout_data'] 	= $this->data['cart']['checkout_data'];
			$this->data['cart'] 			= $this->data['cart']['cart'];
		}
	}

	public function query_cart2($request){
		$this->data['cart'] 			= $this->generateCart2($request);
		if($this->data['cart'] != 'no_data'){
			$this->data['checkout_data'] 	= $this->data['cart']['checkout_data'];
			$this->data['cart'] 			= $this->data['cart']['cart'];
		}
	}
}
