<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;
use digipos\models\Product;
use digipos\models\Discount;
use digipos\models\Flash_deal;
// use Cache;

class ProductController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function index(request $request,$id){
		$this->data['product']				= Product::where('name_alias',$id)
												->with(['product_category.category','product_data_images' => function($query){
													$query->where('status','y');
												},'product_data_attribute_master.product_data_attribute.product_attribute_data.attribute'])
												->where('status','y')
												->first();
		$this->data['product_attribute']	= $this->generate_product_attribute($this->data['product']);
		$discount = Discount::where('product_id', $this->data['product']->id)->first();

		if($discount){
			$this->data['discount'] = $discount;
		} else{
			$this->data['discount'] = '';
		}

		$disco = [];
        $product = Product::join('product_data_attribute_master', 'product.id', 'product_data_attribute_master.product_id')->select('product.*', 'product_data_attribute_master.stock')->where([['product.status', 'y'],['product_data_attribute_master.stock', '>', 0]])->with('thumbnail')->inRandomOrder()->limit(5)->get();

        foreach($product as $q => $pro){
            $disc = Discount::select('product_id', 'amount')->where([['product_id', $pro->id], ['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")]])->first();

            if(count($disc) > 0){
                $disco[$q] = [
                                'product_id'        => $disc->product_id,
                                'discount_amount'   => $disc->amount,
                             ];
            }
        }

        $count_disc = count($disco);

        $this->data['disco_goes'] = $disco > 0 ? $disco : '';

        $count_pro = count($product);

        $this->data['goes_well'] = $count_pro > 0 ? $product : '';

        $this->data['attr'] = product::where('status', 'y')->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();   

		return $this->render_view('pages.product.product');
	}

	public function addCart(request $request){
		$request->request->add(['action' => 'add']);
		$result 				= $this->check_stock($request);
		$status 				= $result['status'];
		$result['type']			= 'danger';
		if($status == 'no_product'){
			$result['text']		= 'Product not found';
		}else if($status == 'available'){
			$this->add_cart($request);

			if(auth()->guard($this->guard)->check()){
				$this->sync_dbase_cart($request);
			}

			$result['text']		= 'Product added to cart';
			$result['type']		= 'success';
		}else if($status == 'no_qty'){
			$result['text']		= 'Stock is not enough';
			$result['type']		= 'danger';
		}else if($status == 'flash_no_qty'){
			$result['text']		= 'Stock is not enough';
			$result['type']		= 'danger';
		}else{
			$result['text']		= 'This attribute is out of stock';
		}
		return response()->json(['result' => $result]);
	}

	public function updateCart(request $request){
		$request->request->add(['action' => 'update']);
		$attribute_master 		= $this->get_attribute($request);		
		$request->request->add(['attr' => $attribute_master->product_data_attribute->pluck('product_attribute_data_id')->toArray()]);
		$request->merge(['id' => $attribute_master->product_id]);
		$result 				= $this->check_stock($request);
		$status 				= $result['status'];
		$result['type']			= 'danger';
		
		if($status == 'no_product'){
			$result['text']		= 'Product not found';
		}else if($status == 'available'){
			$this->update_cart($request);
			$result['text']		= 'Product qty updated';
			$result['type']		= 'success';
		}else if($status == 'no_qty'){
			$result['text']		= 'Stock is not enough';
			$result['type']		= 'danger';
		}else{
			$result['text']		= 'This attribute is out of stock';
		}
		return response()->json(['result' => $result]);
	}

	// Custom update cart
	public function c_updateCart(request $request){
		$result = $this->c_update_cart($request);

		if(auth()->guard($this->guard)->check()){
			$this->sync_dbase_cart($request);
		}

		return response()->json(['result' => $result]);
	}
	// End Custom update cart

	public function removeCart(request $request){
		$id 	= $request->id;	

        $this->remove_cart($id);	

        if(auth()->guard($this->guard)->check()){
			$this->remove_dbase_cart($id);
		}
        return response()->json(['status' => 'success']);
	}

	public function totalCart(){
		$total 	= $this->get_total_cart();
		return $total;
	}

	public function updateCheckoutnotes(request $request){
		$notes 	= $request->notes;
		$this->update_checkout_notes($notes);
		return 'success';
	}
}
