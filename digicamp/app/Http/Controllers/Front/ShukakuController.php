<?php namespace digipos\Http\Controllers\Front;

use Request;
use Cache;
use Cookie;
use Hash;

use digipos\Libraries\Cart;

use digipos\models\Pages;
use digipos\models\Flash_deal;
use digipos\models\Post;
use digipos\models\Config;
use digipos\models\Customer_cart;
use digipos\models\Socialmedia;
use digipos\models\Payment_method;
use digipos\models\Product;
use digipos\models\Product_category;
use digipos\models\Product_data_category;
use digipos\models\Product_attribute_data;
use digipos\models\Product_data_attribute_master;
use digipos\models\Discount;
use digipos\models\Voucher;
use digipos\models\Orderhd;
use digipos\models\Ordervoucher;
use digipos\models\Delivery_service;

class ShukakuController extends Controller {

    public function __construct(){
        parent::__construct();        
    }

    public function initData(){
        //Set Global
        $query_config = Config::where('config_type','front')->orwhere('config_type','both');
        $config = $this->cache_query('config',$query_config,'get');
        foreach($config as $c){
            $this->data[$c->name] = $c->value;
            $this->data['email_config'][$c->name] = $c->value;
        }

        $this->data['social_media']     = $this->cache_query('social_media',Socialmedia::where('status','y')->orderBy('order','asc')->select('image','url'),'get');
        // $this->data['payment_methods']  = $this->cache_query('payment_method',Payment_method::where('status','y')->orderBy('order','asc')->select('payment_method_name','image'),'get');
    }

    public function generateCartIdentifier(){
        $identifier                     = Cookie::get(env('APP_CART_KEY'));
        if(!$identifier){
            $identifier                 = Hash::make(env('APP_CART_KEY')).str_random(3);
        }else{
            $identifier                 = decrypt($identifier);
        }
        
        $cek_identifier                 = Customer_cart::where('identifier',$identifier)->first();
        if(!$cek_identifier){
            $customer_cart              = new Customer_cart;
            $customer_cart->identifier  = $identifier;
            $customer_cart->customer_id = auth()->guard($this->guard)->check() ? auth()->guard($this->guard)->user()->id : '';
            $customer_cart->expired     = date('Y-m-d H:i:s',strtotime('+1 years -6 days'));
            $customer_cart->save();
            Cookie::queue(Cookie::make(env('APP_CART_KEY'),$identifier,1440*30*12));
        }
    }
    
    /*
    public function generateMeta(){
        $this->data['meta_full_image']                  = asset('components/both/images/web/'.$this->data['web_logo']);                    
        $this->data['facebook_meta']['og:title']        = $this->data['web_title'].' | '.$this->data['web_name'];
        $this->data['facebook_meta']['og:site_name']    = $this->data['web_name'];
        $this->data['facebook_meta']['og:url']          = Request::url();
        $this->data['facebook_meta']['og:type']         = "article";
        $this->data['facebook_meta']['og:locale']       = "id_ID";
        $this->data['facebook_meta']['og:image']        = $this->data['meta_full_image'];
        $this->data['facebook_meta']['og:description']  = $this->data['web_description'];

        $this->data['twitter_meta']['twitter:card']          = "summary_large_image";
        $this->data['twitter_meta']['twitter:site']          = "@".$this->data['web_name'];
        $this->data['twitter_meta']['twitter:creator']       = "@".$this->data['web_name'];
        $this->data['twitter_meta']['twitter:url']           = Request::url();
        $this->data['twitter_meta']['twitter:title']         = $this->data['web_name'];
        $this->data['twitter_meta']['twitter:image']         = $this->data['meta_full_image'];
        $this->data['twitter_meta']['twitter:description']   = $this->data['web_description'];
    } */

    // Custom Controller
    public function generateCusMeta(){
        $value = Request::segment(1);
        $value2 = Request::segment(2);

        if($value2 == NULL){
          $meta = Pages::where('pages_name_alias', $value)->first();
        } else if($value2 != NULL){
          $meta = Post::where('post_name_alias', $value2)->first();
        } else{
          $meta = "";
        }

        $default_title = Config::where('name', 'web_title')->first();
        $default_description = Config::where('name', 'web_description')->first();
        $default_keywords = Config::where('name', 'web_keywords')->first();
        
        if($meta != NULL) {
            if($value == $meta->pages_name_alias || $value2 == $meta->post_name_alias) {
                $this->data['met_title'] = $meta->meta_title;
                $this->data['met_description'] = $meta->meta_description;
                $this->data['met_keywords'] = $meta->meta_keywords;

                if($value2 != NULL){
                    $this->data['bg_thumb'] = asset('components/front/images/blog/'.$meta->id.'/'.$meta->thumbnail); 
                }
            } else{
                $this->data['met_title'] = $default_title->value;
                $this->data['met_description'] = $default_description->value;
                $this->data['met_keywords'] = $default_keywords->value;
            }
        } else{
            $this->data['met_title'] = $default_title->value;
            $this->data['met_description'] = $default_description->value;
            $this->data['met_keywords'] = $default_keywords->value;
        }
    }
    
    public function generateMenu(){
        $this->data['menus'] = Pages::where([['status', 'y'], ['header', '!=', NULL]])->orderBy('order', 'asc')->get();
    }

    public function generateCategory(){
        $get_cat = Product_category::where('status', 'y')->where('category_id', 0)->orderBy('id', 'asc')->get();
        $result = [];

        foreach($get_cat as $q => $gets){
            $category = $this->setCategory($gets->id);

            if($category != ""){
                $result[$q] = [
                            'id'            => $gets->id,
                            'name'          => $gets->category_name,
                            'name_alias'    => $gets->category_name_alias,
                            'sub_category'  => $category,
                          ]; 
            } else{
                $check_pro = Product_data_category::where('product_category_id', $gets->id)->count();

                if($check_pro > 0){
                    $result[$q] = [
                                    'id'            => $gets->id,
                                    'name'          => $gets->category_name,
                                    'name_alias'    => $gets->category_name_alias,
                                    'sub_category'  => $category,
                                  ]; 
                }
            }
        }
        $this->data['category_shop']    = $result;
    }

    public function setCategory($gets){
        $get_cat = Product_category::join('product_data_category', 'product_category.id', 'product_data_category.product_category_id')->where('product_category.category_id', $gets)->select('product_category.*')->distinct()->get();

        if(count($get_cat) > 0){
            foreach($get_cat as $k => $gets2){
                $category = $this->setSubCategory($gets2->id);

                $result[$k] = [
                        'id'            => $gets2->id,
                        'name'          => $gets2->category_name,
                        'name_alias'    => $gets2->category_name_alias,
                        'sub_sub_category'  => $category,
                      ]; 
            }
        } else{
            $result = '';
        }

        return $result;
    }

    public function setSubCategory($gets){
        $get_cat = Product_category::join('product_data_category', 'product_category.id', 'product_data_category.product_category_id')->where('product_category.category_id', $gets)->select('product_category.*')->distinct()->get();

        if(count($get_cat) > 0){
            foreach($get_cat as $q => $gets){
                $result[$q] = [
                                'id'            => $gets->id,
                                'name'          => $gets->category_name,
                                'name_alias'    => $gets->category_name_alias,
                              ]; 
            }
        } else{
            $result = '';
        }
        return $result;
    }

    public function generateHeader(){
        $this->data['main_category'] = product_category::with('product_data_category')->where([['category_id', 0],['status', 'y']])->orderBy('id', 'desc')->limit(6)->get();
        $this->data['head_contact'] = Config::where('name', 'phone')->first();
    }

    public function generateFooter(){
       $this->data['footer_category'] = product_category::with('product_data_category', 'subcategory')->where([['category_id', 0], ['status', 'y']])->orderBy('id', 'desc')->get();
       $this->data['social_media'] = Socialmedia::where('status', 'y')->orderBy('order', 'asc')->get();
    }

    public function goes_well(){
        $disco = [];
        $product = Product::join('product_data_attribute_master', 'product.id', 'product_data_attribute_master.product_id')->select('product.*', 'product_data_attribute_master.stock')->where([['product.status', 'y'],['product_data_attribute_master.stock', '>', 0]])->with('thumbnail')->inRandomOrder()->limit(5)->get();

        foreach($product as $q => $pro){
            $disc = Discount::select('product_id', 'amount')->where([['product_id', $pro->id], ['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")]])->first();

            if(count($disc) > 0){
                $disco[$q] = [
                                'product_id'        => $disc->product_id,
                                'discount_amount'   => $disc->amount,
                             ];
            }
        }

        if(count($disco) > 0){
            $this->data['discount'] = $disco;
        } else{
            $this->data['discount'] = '';
        }

        $count_product = count($product);

        $this->data['goes_well'] = $product;

        $this->data['attr'] = product::where('status', 'y')->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();    
    }
    // End Custom Controller

    public function cache_query($name,$query,$type='',$time = 60){
        $c = Cache::remember($name, $time, function() use($query,$type){
            if (!empty($type)){
                if ($type == 'first'){
                    $q = $query->first();
                }else{
                    $q = $query->get();
                }
                return $q;
            }else{
                return $query;
            }
        });
        //Cache::flush();
        return $c;
    }

       public function generate_product_attribute($product){
        $temp_data              = [];
        foreach($this->data['product']->product_data_attribute_master as $pda){
            foreach($pda->product_data_attribute as $pd){
                $product_attribute_data             = $pd->product_attribute_data;
                $product_attribute_data_attribute   = $pd->product_attribute_data->attribute;
                $id             = $product_attribute_data->id;
                $value          = $product_attribute_data->value;
                $order          = $product_attribute_data->order;
                $attribute_name = $product_attribute_data_attribute->attribute_name;
                $attribute_type = $product_attribute_data_attribute->attribute_type;
                if(!array_key_exists($attribute_name, $temp_data)){
                    $temp_data[$attribute_name]         = [
                                                            'name'  => $attribute_name,
                                                            'type'  => $attribute_type,
                                                            'order' => $product_attribute_data_attribute->order
                                                        ];
                }
                if(array_key_exists('data', $temp_data[$attribute_name])){
                    foreach($temp_data[$attribute_name]['data'] as $ada){
                        if($ada['value'] != $value){
                            $temp_data[$attribute_name]['data'][]   = ['id' => $id,'value' => $value,'order' => $order];
                            break;
                        }
                    }
                }else{
                    $temp_data[$attribute_name]['data'][]       = ['id' => $id,'value' => $value,'order' => $order];
                }
            }
        }

        $temp_data  = array_values(array_sort($temp_data, function ($value) {
            return $value['order'];
        }));
        foreach($temp_data as $q => $t){
            $temp_data[$q]['data']  = array_values(array_sort($t['data'], function ($value) {
                return $value['order'];
            }));
        }
        return $temp_data;
    }

    public function check_stock($request){
        $continue = 1;
        $id         = $request['id'];
        $attr       = $request['attr'];
        $qty        = $request['qty'];
        $action     = $request['action'] ?? '';

        $product    = Product::where('id',$id)->with('product_data_attribute_master.product_data_attribute')->first();

        if($request['flash_id'] != ''){
            $get_flash = Flash_deal::select('id','product_id')->where([['id', $request['flash_id']], ['status', 'y'],['tanggal', date('Y-m-d')]])->first();

            if(count($get_flash) > 0){
                $conv_json =  json_decode($get_flash->product_id);
                foreach($conv_json as $c_json){
                    if($c_json->product_id == $id){
                        if($product->product_data_attribute_master[0]->stock > $c_json->stock && $c_json->stock > 0 && $c_json->stock >= $qty){
                            $continue = 1;
                            $request->request->add(['flash_id' => $get_flash->id, 'flash_product' => $c_json->product_id]);
                        } else{
                            $continue = 0;
                        }
                    } 
                }
            }
        }

        if($continue == 1){
            if(!$product){
                return ['status' => 'no_product'];
            }
            foreach($product->product_data_attribute_master as $pda){
                $product_data_attribute     = $pda->product_data_attribute->pluck('product_attribute_data_id')->toArray();
                $check                      = array_diff($attr,$product_data_attribute);
                if(count($check) == 0){
                    $request->request->add(['product_data_attribute_master_id' => $pda->id]);
                    if($action == 'add'){
                        $cur_qty            = Cart::get_qty($pda->id);
                        $qty                += (int)$cur_qty;
                    }else if($action == 'update'){
                        $qty                = (int)$qty;
                    }

                    $product_qty            = $pda->stock;
                    if($product_qty == 0){ 
                        return ['status' => 'out_of_stock'];
                    }else{
                        if($qty > $product_qty){
                            $res    = ['status' => 'no_qty','qty' => $product_qty];
                        }else{
                            $res    = ['status' => 'available','qty' => $product_qty];
                        }
                        return $res;
                    }
                }
            }

            return ['status' => 'attr_not_found'];
        } else{
            return ['status' => 'no_qty'];
        }
        
    }

    // Custom Check Stock 
    public function c_check_stock($request){
        $continue = 1;
        $flash_id = '';
        $flash_product = '';
        $stock = Product_data_attribute_master::where('id', $request->id)->first();

        if($request['flash_id'] != ''){
            $product = Product::where('id',$request['flash_product'])->with('product_data_attribute_master.product_data_attribute')->first();

            $get_flash = Flash_deal::select('id','product_id')->where([['id', $request['flash_id']], ['status', 'y'],['tanggal', '>=', date('Y-m-d')]])->first();

            if(count($get_flash) > 0){
                $conv_json =  json_decode($get_flash->product_id);
                foreach($conv_json as $c_json){
                    if($c_json->product_id == $product->id){
                        if($product->product_data_attribute_master[0]->stock > $c_json->stock){
                            if($c_json->stock >= $request->qty){
                                $continue = 1;
                                $flash_id = $get_flash->id;
                                $flash_product = $c_json->product_id;
                            } else{
                                $continue = 0;                                
                            }
                        } else{
                            $continue = 0;
                        }
                    } 
                }
            }
        }

        if($continue = 1){
            if($stock->stock >= $request->qty){
                $res = ['status' => 'available', 'qty' => $request->qty, 'cr_id' => $request->id, 'flash_id' => $flash_id, 'flash_product' => $flash_product];
            } else {
                $res = ['status' => 'Not Enough', 'qty' => '', 'cr_id' => $request->id,];
            }
        } else{
            $res = ['status' => 'Not Enough', 'qty' => '', 'cr_id' => $request->id,];
        }
        
        return $res;   
    }

    public function ch_check_stock(){
        $check = Cart::get_cart();

        foreach($check as $q => $ch) {
            $stock = Product_data_attribute_master::where('id', $q)->first();

            if($stock->stock >= $ch['qty']) {
                $result[$q] = ['status' => 'available'];
            } else {
                 $result[$q] = ['status' => 'not Enough', 'name' => $ch['data']['name'], 'qty' => $stock->stock];
            }
            
        }

        return $result;
    }

    public function ch_check_voucher($request){
        $id = $request;
        $get_cart =  Cart::get_checkout_data();

        $get_coupon = Voucher::select('total', 'voucher_code', 'min_checkout', 'total_use_per_user')->where([['id', $id],['valid_from', '<=', date('Y-m-d')],['valid_to', '>=', date('Y-m-d')],['status', 'y']])->first();

        if(count($get_coupon) > 0){
            $user = auth()->guard($this->guard)->user()->id;
            $count_coupon = Ordervoucher::where('voucher_code', $get_coupon->voucher_code)->count();
            $count_user = orderhd::join('ordervoucher', 'orderhd.id', 'ordervoucher.orderhd_id')->where('orderhd.customer_id', $user)->count();

            $min_checkout = number_format(($get_coupon->min_checkout),0,',','.');

            if($get_coupon->total_use_per_user <= $count_user) {
                $result = [
                            'id' => 1,
                            'message' => 'Sorry Limit Per user is '.$get_coupon->total_use_per_user.'',
                          ];
            } else if ($get_coupon->total <= $count_coupon){
                $result = [
                            'id' => 1,
                            'message' => 'Sorry Voucher Out of Stock'
                          ];
            } else if($get_coupon->min_checkout > $get_cart['sub_total']){
                $result = [
                            'id' => 1,
                            'message' => 'Sorry min Checkout Rp. '.$get_coupon->min_checkout.'',
                          ];
            } else{
                $result = [
                            'id' => 0,
                            'message' => 'sukses'
                          ];
            }
        } else{
            $result = [
                        'id' => 1,
                        'message' => 'Voucher Not Valid'
                     ];
        }

        return $result;
    }
    // End Custom Stock

    public function check_voucher($request){
       $id = $request['coupon'];
       $coupon = Voucher::where([['voucher_code', $id],['valid_from', '<=', date('Y-m-d')],['valid_to', '>=', date('Y-m-d')],['status', 'y']])->first();
       
       if($coupon != NULL) {
            $vo_used = Ordervoucher::where('voucher_code', $id)->count();
            $checkout_data = Cart::get_checkout_data();
            $user = auth()->guard($this->guard)->user()->id;
            $count_user = orderhd::join('ordervoucher', 'orderhd.id', 'ordervoucher.orderhd_id')->where('orderhd.customer_id', $user)->count();

            if($coupon->total_use_per_user <= $count_user){
                $rs_respon = ['status' => 'failed', 'message' => 'Sorry, Limit per User is '.$coupon->total_use_per_user.''];        
            } else if( $vo_used >= $coupon->total){ 
                $rs_respon = ['status' => 'failed', 'message' => 'Sorry, Voucher stock already empty'];  
            } else if( $checkout_data['total'] <= $coupon->min_checkout){
                $rs_respon = ['status' => 'failed', 'message' => 'Sorry min Checkout Rp '.$coupon->min_checkout.'']; 
            } else if($vo_used <= $coupon->total && $checkout_data['total'] >= $coupon->min_checkout){
                if($coupon->discount_type == "v") {
                    $type = "v";
                    $value = $coupon->amount;
               } else {
                    $type = "p";
                    $value = $coupon->amount / 100;
               }
                $rs_coupon = [
                                 'id' => $coupon->id,
                                 'coupon_name' => $coupon->voucher_name,
                                 'amount' => $coupon->amount,
                                 'value' => $value,
                                 'type' => $type,
                                 'qty' => 1
                               ];

                Cart::set_coupon($rs_coupon);
                $rs_respon = ['status' => 'success'];
            }else{
                $rs_respon = ['status' => 'failed', 'message' => 'Coupon Tidak Valid'];        
            }            

            // if($coupon->total_use_per_user <= $count_user){
            //     $rs_respon = ['status' => 'failed', 'message' => 'SOrry Limit per User is '.$coupon->total_use_per_user.''];
            // } else if($vo_used <= $coupon->total && $checkout_data['total'] >= $coupon->min_checkout) {
            //    if($coupon->discount_type == "v") {
            //         $type = "v";
            //         $value = $coupon->amount;
            //    } else {
            //         $type = "p";
            //         $value = $coupon->amount / 100;
            //    }
            //     $rs_coupon = [
            //                      'id' => $coupon->id,
            //                      'coupon_name' => $coupon->voucher_name,
            //                      'amount' => $coupon->amount,
            //                      'value' => $value,
            //                      'type' => $type,
            //                      'qty' => 1
            //                    ];

            //     Cart::set_coupon($rs_coupon);
            //     $rs_respon = ['status' => 'success'];
            // } else {
            //     $rs_respon = ['status' => 'failed', 'message' => 'Coupon Tidak Valid'];
            // }
       } else{
             $rs_respon = ['status' => 'failed', 'message' => 'Coupon Tidak Valid'];
       }

       return $rs_respon;
    }

    public function generateCart($request){
        $checkout_sub_total = 0;
        $checkout_weight    = 0;
        $total              = 0;
        $coupon             = 0;
        $cart               = Cart::get_cart($request);

        if($cart){
            foreach($cart as $q => $c){
                $id         = $c['product_id'];
                $attr       = $c['attr'];
                $qty        = (int)$c['qty'];
                $weight     = (float)$c['weight'];
                $flash_id       = $c['flash_id'];
                $flash_product  = $c['flash_product'];
                $request->request->add(['id' => $id,'attr' => $attr,'qty' => $qty,'weight' => $weight ,'flash_id' => $flash_id,'flash_product' => $flash_product,'action' => 'generate']);
                $result     = $this->check_stock($request);

                if($result['status'] == 'available'){
                    if($qty > $result['qty']){
                        $this->update_cart(['id' => $id,'attr' => $attr, 'qty' => $result['qty'], 'weight' => $weight, 'product_data_attribute_master_id' => $q]);
                    }
                    $product    = Product::where('id',$id)
                                    ->with('primary_images','product_data_attribute_master.product_data_attribute.product_attribute_data.attribute')
                                    ->where('status','y')
                                    ->first();
                    $attribute          = Product_attribute_data::whereIn('id',$attr)->with('attribute')->get();

                    $flash      = Flash_deal::select('product_id')->where([['id', $flash_id], ['status', 'y'],['tanggal', '>=', date('Y-m-d')]])->first();
                    $flash_price = '';

                    if(count($flash) > 0){
                        $conv_flash = json_decode($flash->product_id);

                        foreach($conv_flash as $cons){
                            if($cons->product_id == $id){
                                $get_disc = $cons->discount / 100;
                                $price = $product->price * $get_disc;
                                $flash_price = $product->price - $price;
                            }
                        }
                    }

                    $temp_attribute     = [];
                    foreach($attribute as $at){
                        $temp_attribute[]   =  [
                                                'attribute' => $at->attribute->attribute_name,
                                                'type'      => $at->attribute->attribute_type,
                                                'value'     => $at->value
                                            ];
                    }
                    
                    if($flash_price == ""){
                        $sub_total  = $product->price*$qty;
                        $prc        = $product->price;
                        $checkout_weight += ($weight * 1000) * $qty;
                        $flashs_id   = '';
                    } else{
                        $sub_total  = $flash_price*$qty;
                        $prc        = $flash_price;
                        $flashs_id   = $flash->id;
                        $checkout_weight += ($weight * 1000) * $qty;
                    }
                
                    $data_product       = [
                        'id'        => $product->id,
                        'images'    => $product->image,
                        'attribute' => $temp_attribute,
                        'name'      => $product->name,
                        'name_alias'=> $product->name_alias,
                        'price'     => (int)$prc,
                        'sub_total' => $sub_total,
                        'flash_id'      => $flashs_id,
                        'flash_product' => $product->id,
                    ];
                    $checkout_sub_total += $sub_total;
                    $request->request->add(['data_product' => $data_product]);
                    Cart::set_product_data($request);
                }else{
                    Cart::remove($q);
                }
            }
            $get_coupon                 = Cart::get_coupon();
            $total                      = $checkout_sub_total;
            $checkout_data              = Cart::get_checkout_data();

            if($get_coupon) {
                if($get_coupon['type'] == "p") {
                    $coupon = $total * $get_coupon['value'];
                    $total = $total - $coupon;
                } else {
                    $coupon = $get_coupon['value'];

                    if($total > $coupon){
                        $total = $total - $coupon;
                    } else if($coupon > $total){
                        $total = 0;
                    }

                }
            }


            $checkout_data = [
                                'sub_total' => $checkout_sub_total,
                                'total_weight' => $checkout_weight,
                                'coupon_id' => $get_coupon['id'] ?? '', 
                                'coupon_name' => $get_coupon['coupon_name'] ?? '',   
                                'coupon' => $coupon,
                                'coupon_amount' => $get_coupon['amount'] ?? '',
                                'coupon_type' => $get_coupon['type'] ?? '',
                                'coupon_qty' => $get_coupon['qty'] ?? '',
                                'total' => $total
                            ];
            
            Cart::set_checkout_data($checkout_data);

            $cart           = Cart::get_cart();       
            $checkout_data  = Cart::get_checkout_data();  
            return ['cart' => $cart,'checkout_data' => $checkout_data];
        }else{
            return 'no_data';
        }
    }

    public function generateCart2($request){
        $checkout_sub_total = 0;
        $checkout_weight    = 0;
        $total              = 0;
        $coupon             = 0;
        $cart               = Cart::get_cart($request);

        if($cart){
            foreach($cart as $q => $c){
                $id             = $c['product_id'];
                $attr           = $c['attr'];
                $qty            = (int)$c['qty'];
                $weight         = (float)$c['weight'];
                $flash_id       = $c['flash_id'];
                $flash_product  = $c['flash_product'];
                $request->request->add(['id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight ,'flash_id' => $flash_id,'flash_product' => $flash_product,'action' => 'generate']);
                $result     = $this->check_stock($request);
                
                $product    = Product::where('id',$id)
                                ->with('primary_images','product_data_attribute_master.product_data_attribute.product_attribute_data.attribute')
                                ->where('status','y')
                                ->first();
                $attribute          = Product_attribute_data::whereIn('id',$attr)->with('attribute')->get();

                $flash      = Flash_deal::select('id','product_id')->where([['id', $flash_id], ['status', 'y'],['tanggal', '>=', date('Y-m-d')]])->first();
                $flash_price = '';

                if(count($flash) > 0){
                    $conv_flash = json_decode($flash->product_id);

                    foreach($conv_flash as $cons){
                        if($cons->product_id == $id){
                            $get_disc = $cons->discount / 100;
                            $price = $product->price * $get_disc;
                            $flash_price = $product->price - $price;
                        }
                    }
                }

                $temp_attribute     = [];
                foreach($attribute as $at){
                    $temp_attribute[]   =  [
                                            'attribute' => $at->attribute->attribute_name,
                                            'type'      => $at->attribute->attribute_type,
                                            'value'     => $at->value
                                        ];
                }

                if($flash_price == ""){
                    $sub_total  = $product->price*$qty;
                    $checkout_weight += ($weight * 1000) * $qty;
                    $prc        = $product->price;
                    $flashs_id   = '';
                } else{
                    $sub_total  = $flash_price*$qty;
                    $checkout_weight += ($weight * 1000) * $qty;
                    $prc        = $flash_price;
                    $flashs_id   = $flash->id;
                }
            
                $data_product       = [
                    'id'        => $product->id,
                    'images'    => $product->image,
                    'attribute' => $temp_attribute,
                    'name'      => $product->name,
                    'name_alias'=> $product->name_alias,
                    'price'     => (int)$prc,
                    'sub_total' => $sub_total,
                    'flash_id'      => $flashs_id,
                    'flash_product' => $product->id,
                ];
                $checkout_sub_total += $sub_total;

                $request->request->add(['data_product' => $data_product]);
                Cart::set_product_data($request);
            }

            $get_coupon                 = Cart::get_coupon();
            $total                      = $checkout_sub_total;
            $checkout_data              = Cart::get_checkout_data();

            if($get_coupon) {
                if($get_coupon['type'] == "p") {
                    $coupon = $total * $get_coupon['value'];
                    $total = $total - $coupon;
                } else {
                    $coupon = $get_coupon['value'];

                    if($total > $coupon){
                        $total = $total - $coupon;
                    } else if($coupon > $total){
                        $total = 0;
                    }

                }
            }

            $checkout_data = [
                                'sub_total' => $checkout_sub_total,
                                'total_weight' => $checkout_weight,
                                'coupon_id' => $get_coupon['id'] ?? '', 
                                'coupon_name' => $get_coupon['coupon_name'] ?? '',   
                                'coupon' => $coupon,
                                'coupon_amount' => $get_coupon['amount'] ?? '',
                                'coupon_type' => $get_coupon['type'] ?? '',
                                'coupon_qty' => $get_coupon['qty'] ?? '',
                                'total' => $total
                            ];
            
            Cart::set_checkout_data($checkout_data);

            $cart           = Cart::get_cart();  
            $checkout_data  = Cart::get_checkout_data();  
            return ['cart' => $cart,'checkout_data' => $checkout_data];
        }else{
            return 'no_data';
        }
    }

    public function sync_dbase_cart($request){
        $customer_cart  = Customer_cart::firstOrCreate(['customer_id' => auth()->guard($this->guard)->user()->id]);
        $temp           = [];
        $cart           = $this->generateCart($request);
        if($cart != 'no_data'){
            foreach($cart['cart'] as $q => $c){
                $temp[] = [
                    'id'            => $q,
                    'product_id'    => $c['product_id'],
                    'attr'          => $c['attr'],
                    'qty'           => $c['qty'],
                    'weight'        => $c['weight'],
                    'flash_id'      => $c['flash_id'],
                    'flash_product' => $c['flash_product'],
                ];
            }
        }
        if($customer_cart->content){
            $dbase_cart     = json_decode($customer_cart->content);
        }else{
            $dbase_cart     = [];  
        }
        foreach($temp as $t){
            $match      = false;
            foreach($dbase_cart as $q => $dc){
                if($dc->id == $t['id']){
                    $dif    = array_diff($t['attr'],$dc->attr);
                    if(count($dif) == 0){
                        $dbase_cart[$q]->qty = $t['qty'];
                        $match  = true;
                        break;
                    }
                }
            }
            if($match == false){
                $dbase_cart[]   = (object)$t;
            }
        }
        foreach($dbase_cart as $q => $dc){
            $data       = [
                'id'        => $dc->product_id,
                'product_data_attribute_master_id'  => $dc->id,
                'attr'      => $dc->attr,
                'qty'       => $dc->qty,
                'weight'    => $dc->weight,
                'flash_id'      => $dc->flash_id,
                'flash_product' => $dc->flash_product,
                'action'    => 'update',
            ];
            $request->request->add($data);
            $check  = $this->check_stock($request);
            if($check['status'] == 'out_of_stock'){
                array_forget($dbase_cart,$q);
            }
            Cart::update($data);
        }
        $customer_cart->content  = json_encode(array_values($dbase_cart));
        $customer_cart->save();
    }

    public function remove_dbase_cart($request){
        $customer_cart  = Customer_cart::where('customer_id', auth()->guard($this->guard)->user()->id)->first();
        if($customer_cart->content){
           $dbase_cart     = json_decode($customer_cart->content); 
       } else{
            $dbase_cart     = [];
       }

       if(count($dbase_cart > 0)){
         foreach($dbase_cart as $q => $rc){
            if($rc->id == $request){
                array_forget($dbase_cart, $q);
            }
         }
         $customer_cart->content = json_encode(array_values($dbase_cart));
         $customer_cart->save();
       }
    }

    public function get_attribute($request){
        $product_data_attribute     = Product_data_attribute_master::where('id',$request['id'])->with('product_data_attribute')->first();
        return $product_data_attribute;
    }

    public function get_total_cart(){
        $total  = Cart::get_total_item();
        return $total;
    }

    public function add_cart($request){
        Cart::add($request);
    }

    public function update_cart($request){
        Cart::update($request);
    }

    public function c_update_cart($request){
        $c_stock = $this->c_check_stock($request);
        $get_cart   = Cart::get_cart();

        if($c_stock['status'] == "available") {
            $id             = $get_cart[$c_stock['cr_id']]['product_id']; 
            $attr           = $get_cart[$c_stock['cr_id']]['attr'];
            $qty            = $c_stock['qty'];
            $weight         = $get_cart[$c_stock['cr_id']]['weight'];
            $flash_id       = $get_cart[$c_stock['cr_id']]['flash_id'];
            $flash_product  = $get_cart[$c_stock['cr_id']]['flash_product'];
            $data       = ['product_id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight, 'flash_id' => $flash_id, 'flash_product' => $flash_product];
            session(['cart.'.$c_stock['cr_id'] => $data]);
            $result = ['status' => 'success'];
        } else{
            $product = Product::where('id', $get_cart[$c_stock['cr_id']]['product_id'])->first();
            $name = $product->name;
            $result = ['status' => 'failed', 'name' => $name];
        }
        return $result;
    }

    public function remove_cart($id){
        Cart::remove($id);
    }

    public function removed_coupon($id){
        Cart::remove_coupon($id);
    }

    public function update_checkout_notes($notes){
        $checkout_data          = Cart::get_checkout_data();
        $checkout_data['notes'] = $notes;
        Cart::set_checkout_data($checkout_data);
    }

    public function delivery_address_type($type){
        $checkout_data                          = Cart::get_checkout_data();
        $checkout_data['delivery_address_type'] = $type;
        Cart::set_checkout_data($checkout_data);
    }

    public function update_delivery_billing_data($data){
        $checkout_data                          = Cart::get_checkout_data();
        $checkout_data['delivery_address_type'] = $data['delivery_type'];
        $checkout_data['delivery_address']      = $data['delivery'];
        $checkout_data['billing_address_type']  = $data['billing_type'];
        $checkout_data['billing_address']       = $data['billing'];
        Cart::set_checkout_data($checkout_data);
    }


    /* Custom Controller */
    public function getprovince($request){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $province = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $province;
        }
    }

    public function getcity($request){
        $curl = curl_init();
        $prov_id = 'province='.$request['province'] ?? '';
        $city_id = 'id='.$request['city'] ?? '';

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/city?".$city_id."&".$prov_id."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $city = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $city;
        }
    }

    public function getsubdistrict($request){
        $curl = curl_init();
        $city_id = 'city='.$request['city'] ?? '';
        $subdis_id = 'id='.$request['sub_district'] ?? '';

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/subdistrict?".$city_id."&".$subdis_id."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $subdistrict = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $subdistrict;
        }
    }

    public function getcourier($request){
       $curl = curl_init();
       $ids = $request['id'];
       $weight = $request['weight'];
       $from = $request['from'];
       $c_courier = '';

        $courier = Delivery_service::where('status', 'y')->get();
        $check = count($courier);

       if($check > 0) {
            foreach($courier as $ct) {
               $get_courier = $ct->name_alias; 
               $c_courier = strtolower($get_courier.':'.$c_courier);
            }
       }

        $get_length = strlen($c_courier) - 1;
        $result = substr($c_courier, 0, $get_length);

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "origin=".$from."&originType=city&destination=".$ids."&destinationType=city&weight=".$weight."&courier=".$result."",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $cost = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $cost;
        }
    }

    public function getwaybill($request){
       $curl = curl_init();
       $waybill = $request['waybill'];
       $courier = $request['courier'];
    
       $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://pro.rajaongkir.com/api/waybill",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "waybill=".$waybill."&courier=".$courier."",
            CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $waybill = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $waybill;
        }
    }
    /* End Custom Controller */
}
