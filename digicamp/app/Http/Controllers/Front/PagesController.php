<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;

use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use Validator;
use PDF;

use App\Mail\MailOrder;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

use digipos\models\Slideshow;
use digipos\models\Banner;
use digipos\models\Pages;
use digipos\models\Post;
use digipos\models\Category;
use digipos\models\Product;
use digipos\models\Product_category;
use digipos\models\Contactmessage;
use digipos\models\Orderstatus;

class PagesController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}
	
	public function sc_live(request $request){
		$search = $request->search;
		$product = Product::where([['status', 'y'],['name', 'LIKE', $search.'%']])->select('name')->get();

		if($product != NULL){
			$result = $product; 
		} else{
			$result['status'] = "No Result";
		}

		return response()->json(['result' => $result]);
	}
	
	public function pages(Request $request){
		$pages = $request->segment(1);
		$slug = $request->segment(2);
		
		$check = Pages::where([['pages_name_alias', $pages], ['header', '!=', NULL]])->first();
		
		if(count($check) > 0) {
			$ids = $check->id;

			if($check->header == "news"){
				if($slug != NULL) {
					$check2 = Post::where('post_name_alias', $slug)->first();

					if(count($check2) > 0){
						$this->detail_news($ids, $slug);
						return $this->render_view('pages.pages.detail_blog');
					}
				} else{
					$this->news($ids);
					return $this->render_view('pages.pages.blog');
				}
			} else if($check->header == "our-company") {
				$this->company($ids);
				return $this->render_view('pages.pages.our_company');
			} else if($check->header == "product") {
				$this->product($ids);
				return $this->render_view('pages.pages.product');
			} else if($check->header == "our-recipes") {
				if($slug != NULL){
					if($slug == "filter"){
						$this->our_recipes_filter($request,$ids);
						return $this->render_view('pages.pages.our_recipes');
					} else{
						$check2 = Post::where('post_name_alias', $slug)->first();

						if(count($check2) > 0){
							$this->our_detail_recipes($ids, $slug);
							return $this->render_view('pages.pages.detail_recipes');
						}
					}
				}else{
					$this->our_recipes($ids);
					return $this->render_view('pages.pages.our_recipes');
				}
			} else if($check->header == "business-chance") {
				$this->business($ids);
				return $this->render_view('pages.pages.business_chance');
			} else if($check->header == "contact-us") {
				$this->contact($ids);
				return $this->render_view('pages.pages.contact_us');
			} 
		} else{
			// $check2 = Product_category::with('product_data_category')->where([['category_name_alias', $pages],['status', 'y']])->orderBy('id', 'desc')->get();

			// if(count($check2) > 0){
			// 	$this->store_page($pages);
			// 	return $this->render_view('pages.pages.store');
			// } else{
			// 	return redirect()->route('homes');
			// }
			return redirect()->route('homes');
		}
	}

	public function pages2(Request $request){
		$pages = $request->segment(1);
		$slug = $request->segment(2);

		$check = Pages::where('pages_name_alias', $pages)->first();

		if(count($check) > 0) {
			$ids = $check->id;

			if($check->header == "contact-us"){
				$this->contact_us($request);
				return redirect()->back()->with('success', 'Thanks For Contact Us, We will Reply you soon');
			}

			if($slug != NULL){
				if($check->header == "our-recipes"){
					if($slug == "filter"){
						$this->our_recipes_filter($request,$ids);
						return $this->render_view('pages.pages.our_recipes');
					} else{
						return redirect()->route('homes');
					}
				} else if($check->header == "news"){
					if($slug == "filter"){
						$this->news_filter($request,$ids);
						return $this->render_view('pages.pages.blog');
					} else{
						return redirect()->route('homes');
					}
				}
			}
		} else{
			return redirect()->route('homes');
		}
	}

	public function store_page($request){
		$ct_name = $request;

		$this->data['slideshow'] = Slideshow::where('status', 'y')->orderBy('id','asc')->get();

		$this->data['product'] = Product_category::with('product_data_category')->where([['category_name_alias', $ct_name],['status', 'y']])->orderBy('id', 'desc')->get();

		$this->data['attr'] = Product::where('status', 'y')->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();

		$this->data['hm_category'] = product_category::with('product_data_category')->where([['category_id', 0],['status', 'y']])->orderBy('id', 'desc')->get();

		$this->data['banner'] = Category::with('subcategory', 'post')->where('id', 10)->get();

		$this->data['video'] = Category::with('subcategory', 'post')->where('id', 12)->OrderBy('id', 'desc')->get();
	}

	public function company($request){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Post::where('category_id', 18)->get();
		$content2 = Post::where('category_id', 19)->get();
		$content3 = Post::where('category_id', 20)->limit(3)->get();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
		$this->data['con2'] = $content2 != NULL ? $content2 : '';
		$this->data['con3'] = $content3 != NULL ? $content3 : '';

	}

	public function product($request){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Category::where('category_id', 29)->get();
		$content2 = Post::join('category', 'category.id', 'post.category_id')->where([['category.category_id', 29], ['category.status', 'y'], ['post.status', 'y']])->select('post.id','post.post_name', 'post.content', 'post.thumbnail', 'post.category_id')->orderBy('post.id', 'desc')->get();
		$content3 = Post::where('category_id', 22)->get();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
		$this->data['con2'] = $content2 != NULL ? $content2 : '';
		$this->data['con3'] = $content3 != NULL ? $content3 : '';
	}

	public function our_recipes($request){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Category::join('post', 'post.category_id', 'category.id')->select('post.*')->where('category.category_id', 24)->paginate(8);
		$content2 = Category::select('id', 'category_name')->where('category_id', 24)->get();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
		$this->data['con2'] = $content2 != NULL ? $content2 : '';
	}

	public function our_detail_recipes($request, $slug){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Post::where('post_name_alias', $slug)->first();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
	}

	public function our_recipes_filter($request, $ids){
		$banner = Banner::where('pages_id', $ids)->first();
		$content2 = Category::select('id', 'category_name')->where('category_id', 24)->get();
		$query = Category::join('post', 'post.category_id', 'category.id')->select('post.*')->where('category.category_id', 24);

		if($request->category != NULL){
			$query->where('category.id', $request->category);
		} 

		if($request->pgscuro != NULL){
			$pgsc = intval($request->pgscuro);
			$content1 = $query->paginate($pgsc);
		} else{
			$content1 = $query->paginate(8);
		}

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
		$this->data['con2'] = $content2 != NULL ? $content2 : '';
	}

	public function business($request){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Post::where('category_id', 28)->get();
		$content2 = Pages::where('id', 14)->select('pages_name_alias')->first();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
		$this->data['con2'] = $content1 != NULL ? $content2 : '';
	}

	public function news($request){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Post::where('category_id', 5)->orderBy('id', 'desc')->paginate(2);

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
	}

	public function news_filter($request, $ids){
		$banner = Banner::where('pages_id', $ids)->first();
		$query = Post::where('category_id', 5)->orderBy('id', 'desc');

		if($request->pgscuro != NULL){
			$pgsc = intval($request->pgscuro);
			$content1 = $query->paginate($pgsc);
		} else{
			$content1 = $query->paginate(5);
		}

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
	}

	public function detail_news($request, $slug){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Post::where('post_name_alias', $slug)->first();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
	}

	public function contact($request){
		$banner = Banner::where('pages_id', $request)->first();
		$content1 = Post::where('category_id', 14)->get();

		$this->data['banner'] = $banner != NULL ? $banner : '';
		$this->data['con1'] = $content1 != NULL ? $content1 : '';
	}	

	public function contact_us($request){
		$this->validate($request,[
			'co_name'							=> 'required',
			'co_email'							=> 'required|email',
			'co_phone'							=> 'required|numeric',
			'co_message'						=> 'required',
			'g-recaptcha-response'				=> 'required',
		],
		[
			'co_name.required'					=> 'Name is required',
			'co_email.required'					=> 'Email is required',
			'co_email.email'					=> 'Email Must be Valid',
			'co_phone.required'					=> 'Phone is required',
			'co_phone.numeric'					=> 'Phone Must Be Numeric',
			'co_message.required'				=> 'Message is required',
			'g-recaptcha-response.required'		=> 'Recaptcha is required',
		]);

		$captcha = $request->input('g-recaptcha-response');
		$secretKey = "6LeqBR4UAAAAAMU4UzWx8_EaOEMARj8UmZhoZ673";
		$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha);
        $responseKeys = json_decode($response,true);
        
        if(intval($responseKeys["success"]) !== 1) {
         	return redirect()->back()->withError('Wrong Captcha, Please Try again');
        } else {
			$contact = new Contactmessage;
			$contact->name = $request->co_name;
			$contact->email = $request->co_email;
			$contact->phone = $request->co_phone;
			$contact->subject = "Contact Us";
			$contact->message = $request->co_message;
			$contact->upd_by = 0;
			$contact->save();

			$mail = Orderstatus::where('id', 31)->first();
			
			if($mail->status_email_cust == 'y'){
				$dt_cust['subject'] = $mail->subject;
				$dt_cust['to'] 		= $request->co_email;

				$this->data['title']        = $mail->title;
				$this->data['status']		= 'cust';
				$this->data['cust_content'] = ucfirst(str_replace('#contact_name',$request->co_name,$mail->email_cust_content));

				Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
	            $message->subject($dt_cust['subject'])
	                  ->to($dt_cust['to']);
	            });
	        }

			if($mail->status_email_admin == 'y') {
				$dt_adm['subject'] 	= $mail->subject;
				$dt_adm['to'] 		= $mail->email_admin;


				$this->data['title']        = $mail->title;
				$this->data['status']		= 'admin';
				
				$change_name = str_replace('#contact_name',$request->co_name,$mail->email_admin_content);
				$change_email = str_replace('#contact_email',$request->co_email,$change_name);
				$change_subject = str_replace('#contact_subject',$request->co_subject,$change_email);
				$change_message = str_replace('#contact_message',$request->co_message,$change_subject);

				$this->data['adm_content'] = $change_message;

				Mail::send('front.mail.content', $this->data, function ($message) use($dt_adm){
		            $message->subject($dt_adm['subject'])
		                  ->to($dt_adm['to']);
		    	});
			}
		}
	}

	public function pdf($ids){
		$data 			= [];
		$data['con1'] 	= Post::where('id', $ids)->first();

		$view 			= $this->view_path.'.pdf.recipes';
		$title			= 'Recipes '.$data['con1']->post_name.'.pdf';
		$pdf 			= PDF::loadView($view, $data);

		return $pdf->setPaper('a4')->download($title);
	}
}
