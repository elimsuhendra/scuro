<?php namespace digipos\Http\Controllers\Front;

use Illuminate\Http\request;
use Validator;
use Hash;
// use digipos\Libraries\Cart;

use digipos\models\Banner;
use digipos\models\Flash_deal;
use digipos\models\Post;
use digipos\models\Slideshow;
use digipos\models\Product;
use digipos\models\Product_category;
use digipos\models\Discount;

use digipos\Libraries\Alert;

class ShopController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function index(request $request){
		// Cart::remove_all();

		$dc_tr = [];
		$dc_tl = [];


		$this->data['product_terbaru'] = Product::with('product_data_attribute_master')->orderBy('product.id', 'desc')->select('product.*')->limit(10)->get();

		foreach($this->data['product_terbaru'] as $q => $prd_tr){
			$get_disc_prd_tr = Discount::select('product_id', 'discount_type', 'amount')->where([['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")],['product_id', $prd_tr->id]])->first();

			if(count($get_disc_prd_tr) > 0){
				$dc_tr[$q] = [
							'product_id' => $prd_tr->id,
							'amount'	 => $get_disc_prd_tr->amount,
						 ];
			}
		}

		$this->data['disc_trb']	= $dc_tr; 

		$this->data['product_discount'] = Product::join('discount', 'discount.product_id', 'product.id')->with('product_data_attribute_master')->orderBy('product.id', 'desc')->select('product.*', 'discount.amount')->where([['product.status', 'y'], ['discount.status', 'y'],['discount.valid_from', '<=', date("Y-m-d")],['discount.valid_to', '>=', date("Y-m-d")]])->limit(8)->inRandomOrder()->get();

		$this->data['product_terlaris'] = Product::join('product_data_attribute_master', 'product_data_attribute_master.product_id', 'product.id')->where('product_data_attribute_master.sold', 1)->orderBy('product_data_attribute_master.sold', 'desc')->select('product.*')->limit(7)->get();

		foreach($this->data['product_terlaris'] as $l => $prd_tl){
			$get_disc_prd_tl = Discount::select('product_id', 'discount_type', 'amount')->where([['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")],['product_id', $prd_tl->id]])->first();

			if(count($get_disc_prd_tl) > 0){
				$dc_tl[$l] = [
							'product_id' => $prd_tl->id,
							'amount'	 => $get_disc_prd_tl->amount,
						 ];
			}
		}

		$this->data['disc_trl']	= $dc_tl; 
		$banner = Banner::where([['status', 'y'], ['pages_id', 16]])->first();

		$this->data['banner'] = count($banner) > 0 ? $banner : ''; 

		$this->data['slideshow'] = Slideshow::where([['status', 'y'], ['pages_id', 16]])->get();

		$this->data['attr'] = Product::where('status', 'y')->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();

		$this->data['sticky_category'] = $this->Sticky_category();

		return $this->render_view('pages.shop.shop');
	}

	public function Sticky_category(){
		$sc = [];
		$i  = 4;
		$k	= 0;
		$cat = Product_category::where([['Sticky_category', 'y'], ['status', 'y']])->limit(4)->orderBy('id','desc')->get();

		if(count($cat) > 0){
			foreach($cat as $scat){
				$sc[$k] = [

							'name'	=> $scat->category_name,
							'url'	=> $scat->category_name_alias,
							'image'	=> $scat->image,
						  ];

				$i--;
				$k++;
			}
		}

		if($i != 0){
			$get_cats = Product_category::where('status', 'y')->limit($i)->inRandomOrder()->get();

			foreach($get_cats as $rcat){
				$sc[$k] = [
							'name'	=> $rcat->category_name,
							'url'	=> $rcat->category_name_alias,
							'image'	=> $rcat->image,
						  ];
				$k++;
			}
		}

		return $sc;
	}

	public function search_live(request $request){
		$search = $request->hd_search;
		$pro 	= [];


		$this->data['title'] = "Search";

		$this->data['product'] = Product::where([['status', 'y'],['name', 'LIKE', $search.'%']])->with('product_data_attribute_master')->paginate(16);

		foreach($this->data['product'] as $q => $prod){
			$disc = Discount::select('product_id', 'amount')->where([['product_id', $prod->id],['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")]])->first();

			$count = count($disc);

			if($count > 0){
				$pro[$q] = [
								'product_id' => $disc->product_id,
								'amount'	 => $disc->amount,
							];
			}
		}

		$this->data['discount']	= $pro;

		$this->data['attr'] = Product::where([['status', 'y'],['name', 'LIKE', $search.'%']])->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();	
		
		return $this->render_view('pages.pages.search');
	}

	public function flash_deal(request $request){
		$rs_flash = [];	
		$nx_flash = [];		
		$ch_date = '';
		$flash_deal = Banner::select('image')->where([['status', 'y'],['pages_id', 17]])->first();

		if(count($flash_deal) > 0){
			$this->data['banner'] =  $flash_deal;
		}

		$get_flash = Flash_deal::select('id','product_id', 'valid_from', 'valid_to', 'tanggal')->where([['status', 'y'],['tanggal', '>=', date('Y-m-d')]])->orderBy('tanggal', 'asc')->get();

		if(count($get_flash) > 0){
			foreach($get_flash as $gt_fl){
				$con_flash = json_decode($gt_fl->product_id);

				if($ch_date == '' || $ch_date == $gt_fl->tanggal){
					foreach($con_flash as $q => $conf){
						$product = Product::where([['status', 'y'],['id', $conf->product_id]])->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->first();

						$rs_flash[$q] = [
										'product_id'   			=> $product->id,
										'product_atr_data_id'  	=> $product->product_data_attribute_master[0]->product_data_attribute[0]->product_attribute_data->id,
										'product_stock'			=> $product->product_data_attribute_master[0]->stock,
										'product_name' 			=> $product->name,
										'weight'				=> $product->weight,
										'discount'	   			=> $conf->discount,
										'valid_to'	   			=> $gt_fl->valid_to,
										'valid_from'   			=> $gt_fl->valid_from,
										'tanggal'				=> $gt_fl->tanggal,
										'price'		   			=> $product->price,
										'flash_id'				=> $gt_fl->id,
										'flash_stock'			=> $conf->stock,
										'image'		   			=> $product->image,
									];
					}

					$ch_date = $gt_fl->tanggal;
				} else {
					foreach($con_flash as $q => $conf){
						$product = Product::where([['status', 'y'],['id', $conf->product_id]])->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->first();

						$nx_flash[$q] = [
										'product_id'   			=> $product->id,
										'product_atr_data_id'  	=> $product->product_data_attribute_master[0]->product_data_attribute[0]->product_attribute_data->id,
										'product_stock'			=> $product->product_data_attribute_master[0]->stock,
										'product_name' 			=> $product->name,
										'weight'				=> $product->weight,
										'discount'	   			=> $conf->discount,
										'valid_to'	   			=> $gt_fl->valid_to,
										'valid_from'   			=> $gt_fl->valid_from,
										'tanggal'				=> $gt_fl->tanggal,
										'price'		   			=> $product->price,
										'flash_stock'			=> $conf->stock,
										'image'		   			=> $product->image,
									];
					}
				}
			}
		}

		$this->data['pro_flash'] = $rs_flash;
		$this->data['next_flash'] = $nx_flash;

		return $this->render_view('pages.shop.flash_deal');
	}

	public function support(request $request){
		$this->data['support'] = Post::where([['category_id', 35],['status', 'y']])->get();
		return $this->render_view('pages.shop.support');
	}

	public function category(request $request){
		$ct_name = $request->segment(3);

		$get_ct = Product_category::select('id', 'category_id', 'category_name' ,'image', 'sub_category')->where('category_name_alias', $ct_name)->first();
		
		$this->data['title'] = $get_ct->category_name;

		$this->data['product'] = Product::join('product_data_category', 'product.id', 'product_data_category.product_id')->with('product_data_attribute_master')->select('product.*')->where([['product.status', 'y'],['product_data_category.product_category_id', $get_ct->id]])->orderBy('product_data_category.id', 'asc')->orderBy('product.id', 'desc')->paginate(16);

		$this->data['discount']	= Discount::select('product_id', 'discount_type', 'amount')->where([['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")]])->get();

		$this->data['attr'] = Product::where('status', 'y')->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();

		return $this->render_view('pages.shop.category_product');
	}
}
